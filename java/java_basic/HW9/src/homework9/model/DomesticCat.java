package homework9.model;

import java.util.Set;

public class DomesticCat extends Pet implements Foul {
    public DomesticCat() {
        setSpecies(Species.DOMESTICCAT);
    }
    public DomesticCat(String nickname) {
        super(nickname);
        setSpecies(Species.DOMESTICCAT);
    }
    public DomesticCat(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
        setSpecies(Species.DOMESTICCAT);
    }
    @Override
    public void respond() {
        System.out.printf("Мур Мяу, хазяїн. Я - %s. Я сьогодні спав весь день! \n", getNickname());
    }
    @Override
    public void foul() {
        System.out.println("Потрібно добре замести сліди...");
    }
}
