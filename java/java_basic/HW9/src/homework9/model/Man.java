package homework9.model;

import java.util.Map;

public final class Man extends Human {
    public Man() {
    }

    public Man(String firstName, String dateOfBirth) {
        super(firstName, dateOfBirth);
    }

    public Man(String firstName, String secondName, String dateOfBirth) {
        super(firstName, secondName, dateOfBirth);
    }

    public Man(String firstName, String secondName, String dateOfBirth, int iq) {
        super(firstName, secondName, dateOfBirth, iq);
    }

    public Man(String firstName, String secondName, String dateOfBirth, int iq, Map<DayOfWeek, String> schedule) {
        super(firstName, secondName, dateOfBirth, iq, schedule);
    }

    @Override
    public String toString() {
        return String.format("Man{name='%s', surname='%s', date=%s, iq=%d, schedule=[%s]}",
                getFirstName(),getSecondName(),describeAge(), getIq(), getSchedule().toString());
    }
    @Override
    public void greetPets() {
        getFamily().getPets().forEach(e -> System.out.printf("Привіт, %s. Батько дома, принеси ка мені капці. \n",
                e.getNickname()));
    }

    public void repairCar(){
        System.out.println("Прийшов час полагодити наше авто. Я пішов в гараж.");
    }
}
