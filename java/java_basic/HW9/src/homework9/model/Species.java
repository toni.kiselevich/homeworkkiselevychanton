package homework9.model;

public enum Species {
    FISH, DOMESTICCAT, DOG, ROBOCAT, UNKNOWN
}
