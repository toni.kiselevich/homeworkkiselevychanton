package homework9.dao;

import homework9.model.Family;

import java.util.ArrayList;
import java.util.List;

public class CollectionFamilyDao implements FamilyDao{
    private final List<Family> families = new ArrayList<>();

    @Override
    public List<Family> getAllFamilies() {
        return this.families;
    }

    @Override
    public Family getFamilyByIndex(int index) {
        if (index >= 0 && index <= families.size()){
            return this.families.get(index);
        }
        return null;
    }

    @Override
    public boolean deleteFamily(int index) {
        if (index >= 0 && index <= families.size()){
            this.families.remove(index);
            return true;
        }
        return false;
    }

    @Override
    public boolean deleteFamily(Family family) {
        return this.families.remove(family);
    }

    @Override
    public void saveFamily(Family family) {
        for (int i = 0; i < this.families.size(); i++) {
            if (this.families.get(i).equals(family)){
                this.families.set(i, family);
                return;
            }
        }
        this.families.add(family);
    }
}
