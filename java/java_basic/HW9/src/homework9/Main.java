package homework9;

import homework9.controller.FamilyController;
import homework9.dao.CollectionFamilyDao;
import homework9.dao.FamilyDao;
import homework9.model.Dog;
import homework9.model.Human;
import homework9.model.Man;
import homework9.model.Woman;
import homework9.service.FamilyService;

public class Main {
    public static void main(String[] args) {
        FamilyDao familyDao = new CollectionFamilyDao();
        FamilyService familyService = new FamilyService(familyDao);
        FamilyController familyController = new FamilyController(familyService);

        familyController.createNewFamily(
                new Woman("Alexa", "Fluterberg", "14/101994"),
                new Man("Kristian", "Adrigos", "11/08/1990")
        );

        familyController.bornChild(familyController.getFamilyById(0), "Kirill", "Alina", true);
        familyController.bornChild(familyController.getFamilyById(0), "Alex", "Veronika", false);
        familyController.adoptChild(familyController.getFamilyById(0), new Human("Olga", "11/08/2002"));

        familyController.createNewFamily(
                new Woman("Katerina", "Vanishcuk", "11/08/1990"),
                new Man("Andrey", "Braga", "11/08/1990")
        );
        familyController.bornChild(familyController.getFamilyById(1), "Vanya", "Nastya", true);



        System.out.println("All families : ");
        familyController.displayAllFamilies();
        System.out.println();

        System.out.println("Family at index №1");
        System.out.println(familyController.getFamilyById(1));
        System.out.println();

        System.out.println("Family with more then one child: ");
        familyController.getFamiliesBiggerThan(1);
        System.out.println();

        System.out.println("Family with less then two children: ");
        familyController.getFamiliesLessThan(2);
        System.out.println();

        System.out.println("Family with three children");
        familyController.countFamiliesWithMemberNumber(3);
        System.out.println();

        System.out.println("All families : ");
        System.out.println(familyController.count());
        System.out.println();

        familyController.addPet(0, new Dog("Archi"));

        System.out.println("All family pets");
        System.out.println(familyController.getPets(0));
        System.out.println();

        System.out.println("Now we deleting all children which older then 2 years");
        familyController.deleteAllChildrenOlderThen(2);
        familyController.displayAllFamilies();
        System.out.println();

        System.out.println("And finally we deleting second family");
        familyController.deleteFamilyByIndex(1);
        familyController.displayAllFamilies();
        System.out.println();
    }
}
