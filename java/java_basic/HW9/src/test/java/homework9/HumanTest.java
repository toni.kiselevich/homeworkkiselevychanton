package homework9;

import homework9.model.DayOfWeek;
import homework9.model.Human;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import static org.junit.jupiter.api.Assertions.assertEquals;

class HumanTest {
    @Test
    void testHumanToStringEmptyConstructor() {
        assertEquals("Human{name='null', surname='null', date=01/01/1970, iq=0, schedule=[{}]}",
                new Human().toString());
    }

    @Test
    void testHumanToStringPartlyFilledConstructor() {
        assertEquals("Human{name='Kiril', surname='Budanov', date=11/08/1985, iq=0, schedule=[{}]}",
                new Human("Kiril", "Budanov", "11/08/1985").toString());
    }

    @Test
    void testdescribeAge() {
        assertEquals("11/08/1985",
                new Human("Kiril", "Budanov", "11/08/1985").describeAge());
    }

    @Test
    void testHumanToStringFullyFilledConstructor() {
        Map<DayOfWeek, String> schedule = new HashMap<>();
        schedule.put(DayOfWeek.MONDAY, "BBQ");
        schedule.put(DayOfWeek.TUESDAY, "GYM");
        schedule.put(DayOfWeek.WEDNESDAY, "DANCE");
        schedule.put(DayOfWeek.THURSDAY, "VOCAL");
        schedule.put(DayOfWeek.FRIDAY, "SWIM");
        schedule.put(DayOfWeek.SATURDAY, "BASKETBALL");
        schedule.put(DayOfWeek.SUNDAY, "CINEMA");

        assertEquals("Human{name='Kiril', surname='Budanov', date=11/08/1985, iq=0, schedule=[{MONDAY=BBQ, TUESDAY=GYM, WEDNESDAY=DANCE, THURSDAY=VOCAL, FRIDAY=SWIM, SATURDAY=BASKETBALL, SUNDAY=CINEMA}]}",
                new Human("Kiril", "Budanov", "11/08/1985", 0, new TreeMap<>(schedule)).toString());
    }
}

