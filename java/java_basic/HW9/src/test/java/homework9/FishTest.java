package homework9;

import homework9.model.Fish;
import org.junit.jupiter.api.Test;

import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FishTest {
    @Test
    void testFishToStringEmptyConstructor(){
        assertEquals("FISH{nickname=null, age=0, trickLevel=0, habits=[[]]}", new Fish().toString());
    }
    @Test
    void testFishToStringPartlyFilledConstructor() {
        assertEquals("FISH{nickname=Bella, age=0, trickLevel=0, habits=[[]]}", new Fish("Bella").toString());
    }
    @Test
    void testFishToStringFullyFilledConstructor() {
        assertEquals("FISH{nickname=Bella, age=5, trickLevel=100, habits=[[]]}",
                new Fish(
                        "Bella",
                        5,
                        190,
                        new HashSet<>()
                ).toString());
    }
}
