package homework9;

import homework9.model.Dog;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DogTest {
    @Test
    void testDogToStringEmptyConstructor() {
        assertEquals("DOG{nickname=null, age=0, trickLevel=0, habits=[[]]}", new Dog().toString());
    }

    @Test
    void testDogToStringPartlyFilledConstructor() {
        assertEquals("DOG{nickname=Bella, age=0, trickLevel=0, habits=[[]]}", new Dog("Bella").toString());
    }

    @Test
    void testDogToStringFullyFilledConstructor() {
        assertEquals("DOG{nickname=Bella, age=5, trickLevel=100, habits=[[Bark, Bite]]}",
                new Dog(
                        "Bella",
                        5,
                        190,
                        new HashSet<>(Arrays.asList("Bark", "Bite"))
                ).toString());
    }
}

