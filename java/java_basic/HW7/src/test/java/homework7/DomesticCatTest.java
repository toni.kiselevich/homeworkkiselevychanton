package homework7;

import homework7.DomesticCat;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DomesticCatTest {
    @Test
    void testDomesticCatToStringEmptyConstructor(){
        assertEquals("DOMESTICCAT{nickname=null, age=0, trickLevel=0, habits=[[]]}", new DomesticCat().toString());
    }
    @Test
    void testDomesticCatToStringPartlyFilledConstructor() {
        assertEquals("DOMESTICCAT{nickname=Bella, age=0, trickLevel=0, habits=[[]]}", new DomesticCat("Bella").toString());
    }
    @Test
    void testDomesticCatToStringFullyFilledConstructor() {
        assertEquals("DOMESTICCAT{nickname=Bella, age=5, trickLevel=100, habits=[[Bark, Bite]]}",
                new DomesticCat(
                        "Bella",
                        5,
                        190,
                        new HashSet<>(Arrays.asList("Bark", "Bite"))
                ).toString());
    }
}
