package homework7;

import static org.junit.jupiter.api.Assertions.assertEquals;

import homework7.Dog;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

class DogTest {
    @Test
    void testDogToStringEmptyConstructor() {
        assertEquals("DOG{nickname=null, age=0, trickLevel=0, habits=[[]]}", new Dog().toString());
    }

    @Test
    void testDogToStringPartlyFilledConstructor() {
        assertEquals("DOG{nickname=Bella, age=0, trickLevel=0, habits=[[]]}", new Dog("Bella").toString());
    }

    @Test
    void testDogToStringFullyFilledConstructor() {
        assertEquals("DOG{nickname=Bella, age=5, trickLevel=100, habits=[[Bark, Bite]]}",
                new Dog(
                        "Bella",
                        5,
                        190,
                        new HashSet<>(Arrays.asList("Bark", "Bite"))
                ).toString());
    }
}

