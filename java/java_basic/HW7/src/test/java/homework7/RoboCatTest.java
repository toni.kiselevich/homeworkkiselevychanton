package homework7;

import homework7.Dog;
import homework7.RoboCat;
import org.junit.jupiter.api.Test;

import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RoboCatTest {
    @Test
    void testDogToStringEmptyConstructor(){
        assertEquals("ROBOCAT{nickname=null, age=0, trickLevel=0, habits=[null]}", new RoboCat().toString());
    }
    @Test
    void testDogToStringPartlyFilledConstructor() {
        assertEquals("ROBOCAT{nickname=Bella, age=0, trickLevel=0, habits=[null]}", new RoboCat("Bella").toString());
    }
    @Test
    void testDogToStringFullyFilledConstructor() {
        assertEquals("ROBOCAT{nickname=Bella, age=5, trickLevel=100, habits=[[Hunt, Jump]]}",
                new RoboCat(
                        "Bella",
                        5,
                        190,
                        new HashSet<>()
                ).toString());
    }
}
