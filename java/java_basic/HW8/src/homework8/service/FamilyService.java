package homework8.service;

import homework8.dao.FamilyDao;
import homework8.model.*;
import sun.util.resources.LocaleData;

import java.time.LocalDate;
import java.time.Period;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class FamilyService {
    private final FamilyDao familyDao;

    public FamilyService(FamilyDao familyDao) {
        this.familyDao = familyDao;
    }

    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }

    public void displayAllFamilies() {

        getAllFamilies().forEach(System.out::println);
    }

    public void getFamiliesBiggerThan(int quantity) {
        familyDao.getAllFamilies().stream()
                .filter(family -> family.getChildren().size() > quantity)
                .forEach(System.out::println);
    }

    public void getFamiliesLessThan(int quantity) {
        familyDao.getAllFamilies().stream()
                .filter(family -> family.getChildren().size() < quantity)
                .forEach(System.out::println);
    }

    public void countFamiliesWithMemberNumber(int quantity) {
        familyDao.getAllFamilies().stream()
                .filter(family -> family.getChildren().size() == quantity)
                .forEach(System.out::println);
    }

    public void createNewFamily(Woman mother, Man father) {
        familyDao.saveFamily(new Family(mother, father));
    }

    public boolean deleteFamilyByIndex(int index) {
        return familyDao.deleteFamily(index);
    }

    public Family bornChild(Family family, String maleName, String femaleName, boolean isMale) {
        family.addChild(isMale ? new Man(maleName, LocalDate.now().getYear()) : new Woman(femaleName, LocalDate.now().getYear()));
        familyDao.saveFamily(family);
        return family;
    }

    public Family adoptChild(Family family, Human child) {
        family.addChild(child);
        familyDao.saveFamily(family);
        return family;
    }

    public void deleteAllChildrenOlderThen(int age) {
        familyDao.getAllFamilies().
                forEach(family -> {
                    List<Human> children = family.getChildren()
                            .stream()
                            .filter(child -> Period.between(LocalDate.ofYearDay(child.getYearOfBirth(), 1), LocalDate.now()).getYears() < age)
                            .collect(Collectors.toList());

                    family.setChildren(children);
                });
    }
    public int count(){
        return familyDao.getAllFamilies().size();
    }
    public Family getFamilyById(int index){
       return familyDao.getFamilyByIndex(index);
    }
    public Set<Pet> getPets(int index){
        return familyDao.getFamilyByIndex(index).getPets();
    }
    public void addPet(int index, Pet newPet){
        familyDao.getFamilyByIndex(index).addPet(newPet);
    }
}

