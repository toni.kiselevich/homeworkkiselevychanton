package homework8.model;

public enum Species {
    FISH, DOMESTICCAT, DOG, ROBOCAT, UNKNOWN
}
