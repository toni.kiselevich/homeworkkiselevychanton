package homework8.model;

import java.util.Map;

public final class Woman extends Human{
    public Woman() {
    }

    public Woman(String firstName, int yearOfBirth) {
        super(firstName, yearOfBirth);
    }

    public Woman(String firstName, String secondName, int yearOfBirth) {
        super(firstName, secondName, yearOfBirth);
    }

    public Woman(String firstName, String secondName, int yearOfBirth, int iq, Map<DayOfWeek, String> schedule) {
        super(firstName, secondName, yearOfBirth, iq, schedule);
    }

    @Override
    public String toString() {
        return String.format("Woman{name='%s', surname='%s', year=%d, iq=%d, schedule=[%s]}",
                getFirstName(),getSecondName(),getYearOfBirth(), getIq(), getSchedule().toString());
    }
    @Override
    public void greetPets() {
        getFamily().getPets().forEach(e -> System.out.printf("Привіт, %s. Мати дома, пішли на кухню погодую тебе. \n",
                e.getNickname()));
    }

    public void makeup(){
        System.out.println("Я майже готова, залишилось нанести помаду на губи.");
    }
}
