package homework8.model;

import java.util.Map;

public final class Man extends Human {
    public Man() {
    }

    public Man(String firstName, int yearOfBirth) {
        super(firstName, yearOfBirth);
    }

    public Man(String firstName, String secondName, int yearOfBirth) {
        super(firstName, secondName, yearOfBirth);
    }

    public Man(String firstName, String secondName, int yearOfBirth, int iq, Map<DayOfWeek, String> schedule) {
        super(firstName, secondName, yearOfBirth, iq, schedule);
    }

    @Override
    public String toString() {
        return String.format("Man{name='%s', surname='%s', year=%d, iq=%d, schedule=[%s]}",
                getFirstName(),getSecondName(),getYearOfBirth(), getIq(), getSchedule().toString());
    }
    @Override
    public void greetPets() {
        getFamily().getPets().forEach(e -> System.out.printf("Привіт, %s. Батько дома, принеси ка мені капці. \n",
                e.getNickname()));
    }

    public void repairCar(){
        System.out.println("Прийшов час полагодити наше авто. Я пішов в гараж.");
    }
}
