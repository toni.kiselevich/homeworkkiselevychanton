package homework8;



import homework8.dao.CollectionFamilyDao;
import homework8.dao.FamilyDao;
import homework8.model.Family;
import homework8.model.Man;
import homework8.model.Woman;


import homework8.service.FamilyService;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

class FamilyServiceTest {
    FamilyDao familyDao;
    FamilyService familyService;
    @BeforeEach
    public void setUp(){
         familyDao = new CollectionFamilyDao();
         familyService = new FamilyService(familyDao);
    }

    @Test
    void createFamilyTest(){
        Woman mother = new Woman("Jane", 2000);
        Man father = new Man("John", 1999);

        familyService.createNewFamily(mother, father);
        assertEquals(1, familyService.count());
    }

    @Test
    void deleteFamilyByIndex(){
        Woman mother = new Woman("Jane", 2000);
        Man father = new Man("John", 1999);

        familyService.createNewFamily(mother, father);
        assertEquals(1, familyService.count());
        familyService.deleteFamilyByIndex(0);
        assertEquals(0, familyService.count());
    }

    @Test
    void bornChildTest(){
        Woman mother = new Woman("Jane", 2000);
        Man father = new Man("John", 1999);

        familyService.createNewFamily(mother, father);
        Family fam = familyService.getFamilyById(0);
        assertEquals(0, fam.getChildren().size());
        familyService.bornChild(fam,"Andrey", "Olga", true);
        assertEquals(1, fam.getChildren().size());
    }

    @Test
    void adoptChildTest(){
        Woman mother = new Woman("Jane", 2000);
        Man father = new Man("John", 1999);

        familyService.createNewFamily(mother, father);
        Family fam = familyService.getFamilyById(0);
        assertEquals(0, fam.getChildren().size());

        Man child = new Man("Kirill", "Avgustinovich", 2009);

        familyService.adoptChild(fam, child);
        assertEquals(1, fam.getChildren().size());
        assertEquals(true, fam.getChildren().contains(child));
    }

    @Test
    void deleteAllChildrenOlderThenTest(){
        Woman mother = new Woman("Jane", 2000);
        Man father = new Man("John", 1999);

        familyService.createNewFamily(mother, father);
        Family fam = familyService.getFamilyById(0);
        assertEquals(0, fam.getChildren().size());

        Man child = new Man("Kirill", "Avgustinovich", 2009);
        Man child2 = new Man("Evgen", "Avgustinovich", 2004);
        Man child3 = new Man("Arsen", "Avgustinovich", 2022);

        familyService.adoptChild(fam, child);
        familyService.adoptChild(fam, child2);
        familyService.adoptChild(fam, child3);
        assertEquals(3, fam.getChildren().size());

        familyService.deleteAllChildrenOlderThen(2);
        assertEquals(1, fam.getChildren().size());
    }


}

