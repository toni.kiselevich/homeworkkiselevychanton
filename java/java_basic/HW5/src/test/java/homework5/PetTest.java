package homework5;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class PetTest {
    @Test
    void testPetToStringEmptyConstructor(){
        assertEquals("null{nickname=null, age=0, trickLevel=0, habits=[null]}", new Pet().toString());
    }
    @Test
    void testToStringPartlyFilledConstructor() {
        assertEquals("DOG{nickname=Bella, age=0, trickLevel=0, habits=[null]}", new Pet(Species.DOG, "Bella").toString());
    }
    @Test
    void testToStringFullyFilledConstructor() {
        assertEquals("DOG{nickname=Bella, age=5, trickLevel=100, habits=[[Hunt, Jump]]}",
                new Pet(Species.DOG,
                        "Bella",
                        5,
                        190,
                        new String[]{"Hunt", "Jump"}
                        ).toString());
    }
}

