package homework5;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class HumanTest {
    @Test
    void testHumanToStringEmptyConstructor() {
        assertEquals("Human{name='null', surname='null', year=0, iq=0, schedule=[null]",
                new Human().toString());
    }
    @Test
    void testHumanToStringPartlyFilledConstructor() {
        assertEquals("Human{name='Kiril', surname='Budanov', year=1985, iq=0, schedule=[null]",
                new Human("Kiril", "Budanov", 1985).toString());
    }
    @Test
    void testHumanToStringFullyFilledConstructor() {
        assertEquals("Human{name='Kiril', surname='Budanov', year=1985, iq=100, schedule=[[[MONDAY, BBQ], [TUESDAY, GYM], [WEDNESDAY, DANCE], [THURSDAY, VOCAL], [FRIDAY, SWIM], [SATURDAY, BASKETBALL], [SUNDAY, CINEMA]]]",
                new Human("Kiril",
                        "Budanov",
                        1985,
                        100,
                        new String[][]{{DayOfWeek.MONDAY.name(), "BBQ"},{
                                DayOfWeek.TUESDAY.name(), "GYM"
                        },{
                                DayOfWeek.WEDNESDAY.name(), "DANCE"
                        },{
                                DayOfWeek.THURSDAY.name(), "VOCAL"
                        },{
                                DayOfWeek.FRIDAY.name(), "SWIM"
                        },{
                                DayOfWeek.SATURDAY.name(), "BASKETBALL"
                        },{
                                DayOfWeek.SUNDAY.name(), "CINEMA"
                        }}
                        ).toString());
    }
}

