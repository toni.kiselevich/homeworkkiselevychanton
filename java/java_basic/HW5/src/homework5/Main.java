package homework5;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        for (int i = 0; i < 1_000_000; i++) {
            new Human(
                    "Anna",
                    "Kirilova",
                    i,
                    i,
                    new String[][]{{DayOfWeek.MONDAY.name(), "BBQ"},{
                            DayOfWeek.TUESDAY.name(), "GYM"
                    },{
                            DayOfWeek.WEDNESDAY.name(), "DANCE"
                    },{
                            DayOfWeek.THURSDAY.name(), "VOCAL"
                    },{
                            DayOfWeek.FRIDAY.name(), "SWIM"
                    },{
                            DayOfWeek.SATURDAY.name(), "BASKETBALL"
                    },{
                            DayOfWeek.SUNDAY.name(), "CINEMA"
                    }}
            );
        }
    }
}
