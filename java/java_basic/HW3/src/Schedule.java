import java.util.Scanner;

public class Schedule {
    public static void main(String[] args) {
        String[] weekDays = {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};
        String[] tasks = {"Do homework", "Go to the gym", "Watch a movie", "Visit hospital", "Go to family dinner", "Clean house", "Play games"};
        String[][] schedule = new String[7][2];
        for (int i = 0; i < schedule.length; i++) {
            schedule[i][0] = weekDays[i];
            schedule[i][1] = tasks[i];
        }

        Scanner scanner = new Scanner(System.in);

        String pickedDay;
        int index;
        while (true) {
            System.out.println("Please, input the day of the week: ");
            pickedDay = scanner.nextLine().trim().toUpperCase();
            index = findIndexOfDay(pickedDay, schedule);

            switch (pickedDay) {
                case "MONDAY":
                    System.out.println(schedule[index][1]);
                    break;
                case "TUESDAY":
                    System.out.println(schedule[index][1]);
                    break;
                case "WEDNESDAY":
                    System.out.println(schedule[index][1]);
                    break;
                case "THURSDAY":
                    System.out.println(schedule[index][1]);
                    break;
                case "FRIDAY":
                    System.out.println(schedule[index][1]);
                    break;
                case "SATURDAY":
                    System.out.println(schedule[index][1]);
                    break;
                case "SUNDAY":
                    System.out.println(schedule[index][1]);
                    break;
                case "EXIT":
                    System.exit(0);
                default:
                    System.out.println("Sorry, I don't understand you, please try again.");
                    break;
            }
        }
    }

    public static int findIndexOfDay(String day, String[][] schedule) {
        int index = 0;
        for (int i = 0; i < schedule.length; i++) {
            if (schedule[i][0].toUpperCase().equals(day)) {
                index = i;
            }
        }
        return index;
    }
}
