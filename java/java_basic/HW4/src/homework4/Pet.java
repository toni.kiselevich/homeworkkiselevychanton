package homework4;

import java.util.Arrays;

public class Pet {
    private String species;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;

    public Pet(){

    }
    public Pet(String species, String nickname){
        this.species = species;
        this.nickname = nickname;
    }
    public Pet(String species, String nickname, int age, int trickLevel, String[] habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.habits = habits;
        if (trickLevel >= 0 && trickLevel <= 100){
            this.trickLevel = trickLevel;
        } else if (trickLevel > 100) {
            this.trickLevel = 100;
        } else {
            this.trickLevel = 0;
        }
    }

    public String getSpecies() {
        return species;
    }

    public String getNickname() {
        return nickname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if (age > this.age){
            this.age = age;
        }
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public String getTrickLevelByString() {
        return this.trickLevel > 50 ? "дуже хитрий" : "майже не хитрий";
    }

    public void setTrickLevel(int trickLevel) {
        if (trickLevel >= 0 && trickLevel <= 100){
            this.trickLevel = trickLevel;
        } else if (trickLevel > 100) {
            this.trickLevel = 100;
        } else {
            this.trickLevel = 0;
        }
    }

    public String[] getHabits() {
        return habits;
    }

    public void eat(){
        System.out.println("Я ї'м!");
    }

    public void respond(){
        System.out.printf("Привіт, хазяїн. Я - %s. Я скучив!", this.nickname);
    }

    public void foul(){
        System.out.println("Потрібно добре замести сліди...");
    }


    @Override
    public String toString() {
        return String.format("%s{nickname=%s, age=%d, trickLevel=%d, habits=[%s]}",
                getSpecies(),getNickname(),getAge(), getTrickLevel(), Arrays.toString(getHabits()));
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Pet pet = (Pet) o;

        if (getAge() != pet.getAge()) return false;
        if (!getSpecies().equals(pet.getSpecies())) return false;
        return getNickname().equals(pet.getNickname());
    }

    @Override
    public int hashCode() {
        int result = getSpecies() != null ? getSpecies().hashCode() : 0;
        result = 31 * result + (getNickname() != null ? getNickname().hashCode() : 0);
        result = 31 * result + getAge();
        result = 31 * result + getTrickLevel();
        result = 31 * result + Arrays.hashCode(getHabits());
        return result;
    }
}
