package homework4;

import java.util.Arrays;

public class Human {
    private String firstName;
    private String secondName;
    private int yearOfBirth;
    private int iq;
    private Family family;

    private String[][] schedule;

    public Human(){

    }

    public Human(String firstName, String secondName, int yearOfBirth){
        this.firstName = firstName;
        this.secondName = secondName;
        this.yearOfBirth = yearOfBirth;
    }

    public Human(String firstName, String secondName, int yearOfBirth, int iq, String[][] schedule) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.yearOfBirth = yearOfBirth;
        this.schedule = schedule;

        if (iq >= 0 && iq <= 100){
            this.iq = iq;
        } else if (iq > 100) {
            this.iq = 100;
        } else {
            this.iq = 0;
        }
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public int getYearOfBirth() {
        return yearOfBirth;
    }

    public int getIq() {
        return iq;
    }

    public String[][] getSchedule() {
        return schedule;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public void setYearOfBirth(int yearOfBirth) {
        this.yearOfBirth = yearOfBirth;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }

    public void greetPet(){
        System.out.printf("Привіт, %s \n", this.family.getPet().getNickname());
    }

    public void describePet(){
        System.out.printf("У мене є %s, їй %d років, він %s \n",
                this.family.getPet().getNickname(), this.family.getPet().getAge(), this.family.getPet().getTrickLevelByString());
    }


    @Override
    public String toString() {
        return String.format("Human{name='%s', surname='%s', year=%d, iq=%d, schedule=[%s]",
                getFirstName(),getSecondName(),getYearOfBirth(), getIq(), Arrays.deepToString(getSchedule()));
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Human human = (Human) o;

        if (getYearOfBirth() != human.getYearOfBirth()) return false;
        if (getIq() != human.getIq()) return false;
        if (!getFirstName().equals(human.getFirstName())) return false;
        if (!getSecondName().equals(human.getSecondName())) return false;
        if (!family.equals(human.family)) return false;

        return Arrays.deepEquals(getSchedule(), human.getSchedule());
    }

    @Override
    public int hashCode() {
        int result = getFirstName().hashCode();
        result = 31 * result + getSecondName().hashCode();
        result = 31 * result + getYearOfBirth();
        result = 31 * result + getIq();
        result = 31 * result + family.hashCode();
        result = 31 * result + Arrays.hashCode(getSchedule());
        return result;
    }
}
