package homework4;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Family firstFam = new Family(
                new Human("Anna", "Korneeva", 1999),
                new Human("Kiril", "Voronin", 1999)
        );
        System.out.println("FIRST FAMILY :");
        System.out.println(firstFam.getMother().toString());
        System.out.println(firstFam.getFather().toString());
        System.out.println();

        Family secondFam = new Family(
                new Human("Katerina", "Loboda", 2000, 99, new String[][]{{"Monday", "Swim"}}),
                new Human("Evgen", "Akord", 1996, 150, new String[][]{{"Tuesday", "Gym"}})
        );
        System.out.println("SECOND FAMILY :");
        System.out.println(secondFam.getMother().toString());
        System.out.println(secondFam.getFather().toString());
        System.out.println();

        Family thirdFam = new Family(
                new Human(),
                new Human()
        );
        System.out.println("THIRD FAMILY :");
        System.out.println(thirdFam.getMother().toString());
        System.out.println(thirdFam.getFather().toString());
        System.out.println();


        System.out.println("FOURTH FAMILY");
        System.out.println();

        Human mother = new Human("Olga", "Vecher", 1995);
        Human father = new Human("Leonid", "Davidov", 1991);
        Human child = new Human("Daria", "Martova", 1999);
        Pet pet = new Pet("Dog", "Shurik");
        Family family = new Family(mother, father);
        family.setPet(pet);
        family.addChild(child);

        child.describePet();
        child.greetPet();
        child.setIq(199);
        child.setSchedule(new String[][]{{"Monday", "Do homework"},{
                "Tuesday", "Go to the gym"
        },{
                "Wednesday", "Watch a movie"
        },{
                "Thursday", "Visit hospital"
        },{
                "Friday", "Go to family dinner"
        },{
                "Saturday", "Clean house"
        },{
                "Sunday", "Play games"
        }});
        System.out.println(child);
    }
}
