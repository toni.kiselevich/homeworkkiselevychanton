package homework12.controller;

import homework12.exceptions.FamilyOverflowException;
import homework12.model.*;
import homework12.service.FamilyService;

import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class FamilyController {
    private final static Scanner scanner = new Scanner(System.in);
    private final FamilyService familyService;

    public FamilyController(FamilyService familyService) {
        this.familyService = familyService;
    }


    public void createMockFamilies() {
        familyService.createMockFamilies();
    }

    public List<Family> getAllFamilies() {
        return familyService.getAllFamilies();
    }

    public void displayAllFamilies() {
        familyService.displayAllFamilies();
    }

    public void getFamiliesBiggerThan() {

        System.out.print("Введи кількість людей в сім'ї: ");
        int moreThenQuantity = scanner.nextInt();
        System.out.println("Всі сім'ї де кількість людей більша ніж " + moreThenQuantity + ": ");
        familyService.getFamiliesBiggerThan(moreThenQuantity);
    }

    public void getFamiliesLessThan() {
        System.out.print("Введи кількість людей в сім'ї: ");
        int lessThenQuantity = scanner.nextInt();
        System.out.println("Всі сім'ї де кількість людей менша ніж " + lessThenQuantity + ": ");
        familyService.getFamiliesLessThan(lessThenQuantity);
    }

    public void countFamiliesWithMemberNumber() {
        System.out.print("Введи кількість людей в сім'ї: ");
        int quantity = scanner.nextInt();
        System.out.println("Всі сім'ї де кількість людей менша ніж " + quantity + ": ");
        familyService.countFamiliesWithMemberNumber(quantity);
    }

    public void createNewFamily() {
        System.out.println("Для створення родини мені знадобиться деяка інформація: ");

        System.out.print("Ім'я матері: ");
        scanner.next();
        String motherFirstName = scanner.nextLine();
        System.out.println();

        System.out.print("Прізвище матері: ");
        String motherSecondName = scanner.nextLine();
        System.out.println();

        System.out.print("Рік народження матері: ");
        String motherYearOfBirth = scanner.nextLine();
        System.out.println();

        System.out.print("Місяць народження матері: ");
        String motherMonthOfBirth = scanner.nextLine();
        System.out.println();

        System.out.print("День народження матері: ");
        String motherDayOfBirth = scanner.nextLine();
        System.out.println();

        String motherDataOfBirth = String.format("%s/%s/%s", motherDayOfBirth, motherMonthOfBirth, motherYearOfBirth);

        System.out.print("IQ матері: ");
        int motherIq = scanner.nextInt();


        System.out.print("Ім'я батька: ");
        scanner.next();
        String fatherFirstName = scanner.nextLine();
        System.out.println();

        System.out.print("Прізвище батька: ");
        String fatherSecondName = scanner.nextLine();
        System.out.println();

        System.out.print("Рік народження батька: ");
        String fatherYearOfBirth = scanner.nextLine();
        System.out.println();

        System.out.print("Місяць народження батька: ");
        String fatherMonthOfBirth = scanner.nextLine();
        System.out.println();

        System.out.print("День народження батька: ");
        String fatherDayOfBirth = scanner.nextLine();
        System.out.println();

        String fatherDataOfBirth = String.format("%s/%s/%s", fatherDayOfBirth, fatherMonthOfBirth, fatherYearOfBirth);

        System.out.print("IQ батька: ");
        int fatherIq = scanner.nextInt();
//        scanner.nextLine();

        familyService.createNewFamily(
                new Woman(motherFirstName, motherSecondName, motherDataOfBirth, motherIq),
                new Man(fatherFirstName, fatherSecondName, fatherDataOfBirth, fatherIq)
        );

        System.out.println("Сім'ю створено");
    }

    public boolean deleteFamilyByIndex() {
        System.out.println("Введіть ІД сім'ї яку бажаєте видалити: ");
        int index = scanner.nextInt();
        return familyService.deleteFamilyByIndex(index);
    }

    public Family bornChild() {
        System.out.println("Введіть індекс сім'ї");
        Family family = getFamilyById(scanner.nextInt());
        if (family.getChildren().size() == 2) {
            throw new FamilyOverflowException("Ви перевищили максимальну кількість дітей !");
        }
        System.out.println("Введіть хлопчакове ім'я");
        scanner.nextLine();
        String maleName = scanner.nextLine();
        System.out.println("Введіть дівчаче ім'я");
        String femaleName = scanner.nextLine();
        System.out.println("Напишіть true якщо хочете хлопчика, або false якщо дівчинку");
        boolean isMale = scanner.nextBoolean();
        scanner.nextLine();
        return familyService.bornChild(family, maleName, femaleName, isMale);
    }

    public Family adoptChild() {
        System.out.println("Введіть індекс сім'ї");
        Family family = getFamilyById(scanner.nextInt());
        if (family.getChildren().size() == 2) {
            throw new FamilyOverflowException("Ви перевищили максимальну кількість дітей !");
        }
        System.out.println("Введіть ім'я");
        scanner.nextLine();
        String name = scanner.nextLine();
        System.out.println("Введіть дату народження у форматі День/Місяць/Рік ");
        String dateOfBirth = scanner.nextLine();
        Human child = new Man(name, dateOfBirth);
        return familyService.adoptChild(family, child);
    }

    public void deleteAllChildrenOlderThen() {
        System.out.println("Після якого віку видалити всіх дітей хто старше ?");
        int age = scanner.nextInt();
        familyService.deleteAllChildrenOlderThen(age);
    }

    public int count() {
        return familyService.count();
    }

    public Family getFamilyById(int index) {
        return familyService.getFamilyById(index);
    }

    public Set<Pet> getPets(int index) {
        return familyService.getPets(index);
    }

    public void addPet(int index, Pet newPet) {
        familyService.addPet(index, newPet);
    }

    public void menu() {

        System.out.println("Greetings, to continue, choose the option from the list below: ");

        String menu = """
                1. Заповнити тестовими даними
                2. Відобразити весь список сімей
                3. Відобразити список сімей, де кількість людей більша за задану
                4. Відобразити список сімей, де кількість людей менша за задану
                5. Підрахувати кількість сімей, де кількість членів дорівнює
                6. Створити нову родину
                7. Видалити сім'ю за індексом сім'ї у загальному списку
                8. Редагувати сім'ю за індексом сім'ї у загальному списку
                9. Видалити всіх дітей старше віку (у всіх сім'ях видаляються діти старше зазначеного віку
                0. Завершити роботу
                """;

        int option;

        while (true) {
            System.out.println(menu);
            System.out.print("Твій вибір: ");

            if (!scanner.hasNextInt()) {
                System.out.println("Ти повинен ввести число: ");
                scanner.next();
                continue;
            }

            option = scanner.nextInt();

            switch (option) {
                case 1:
                    createMockFamilies();
                    System.out.println("Сім'ї створені");
                    break;
                case 2:
                    displayAllFamilies();
                    break;
                case 3:
                    getFamiliesBiggerThan();
                    break;
                case 4:
                    getFamiliesLessThan();
                    break;
                case 5:
                    countFamiliesWithMemberNumber();
                    break;
                case 6:
                    try {
                        createNewFamily();
                    } catch (Exception e){
                        System.out.println("Щось пішло не по плану: " + e.getMessage());
                    }
                    break;
                case 7:
                    System.out.println(deleteFamilyByIndex() ? "Сім'ю видалено" : "Сім'ю не видалено");
                    break;
                case 8:
                    System.out.println("Якщо хочете народити дитину натисніть 1, або якщо усиновити дитину натисніть 2, або 3 щоб вийти");
                    try {
                        switch (scanner.nextInt()) {
                            case 1:
                                System.out.println(bornChild().prettyFormat());
                                break;
                            case 2:
                                System.out.println(adoptChild().prettyFormat());
                                break;
                            case 3:
                                break;
                            default:
                                break;
                        }
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                    break;
                case 9:
                    deleteAllChildrenOlderThen();
                    System.out.println("Дітей видалено");
                    break;
                case 0:
                    System.exit(0);
                    break;
                default:
                    break;

            }

        }

    }
}
