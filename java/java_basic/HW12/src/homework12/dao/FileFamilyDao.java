package homework12.dao;

import homework12.model.Family;

import java.io.*;
import java.util.*;

public class FileFamilyDao implements FamilyDao{
    private final List<Family> families = new LinkedList<>();
    private static final String LIST_FILE_NAME = "family.ser";

    public FileFamilyDao(){
        loadFamilies();
    }
    @Override
    public List<Family> getAllFamilies() {
        return this.families;
    }

    @Override
    public Family getFamilyByIndex(int index) {
        if (index >= 0 && index <= families.size()){
            return this.families.get(index);
        }
        return null;
    }

    @Override
    public boolean deleteFamily(int index) {
        if (index >= 0 && index <= families.size()){
            this.families.remove(index);
            saveFamilies();
            return true;
        }
        return false;
    }

    @Override
    public boolean deleteFamily(Family family) {
        boolean result = this.families.remove(family);
        if (result){
            saveFamilies();
        }
        return result;
    }

    @Override
    public void createFamily(Family family) {
        for (int i = 0; i < this.families.size(); i++) {
            if (this.families.get(i).equals(family)){
                this.families.set(i, family);
                return;
            }
        }
        this.families.add(family);
        saveFamilies();
    }
    @Override
    public void saveFamilies() {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(LIST_FILE_NAME))) {
            oos.writeObject(families);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void loadFamilies() {
        File file = new File(LIST_FILE_NAME);
        if (file.exists()) {
            try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))) {
                Object readObject = ois.readObject();
                if (readObject instanceof List) {
                    families.addAll((List<Family>) readObject);
                }
            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
}
