package homework12.dao;

import homework12.model.Family;

import java.util.List;

public interface FamilyDao {
    List<Family> getAllFamilies();
    Family getFamilyByIndex(int index);
    boolean deleteFamily(int index);
    boolean deleteFamily(Family family);
    void createFamily(Family family);
    void saveFamilies();
    void loadFamilies();
}
