package homework12.model;

import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.TreeMap;

public class Human implements Serializable {
    private String firstName;
    private String secondName;
    private long dateOfBirth;
    private int iq;
    private Family family;

    private TreeMap<DayOfWeek, String> schedule;

    public Human(){
        this.schedule = new TreeMap<>();
    }

    public Human(String firstName, String dateOfBirth){
        this.firstName = firstName;
        this.dateOfBirth = LocalDate.parse(dateOfBirth,DateTimeFormatter.ofPattern("dd/MM/yyyy")).atStartOfDay(ZoneId.of("GMT")).toInstant().toEpochMilli();
        this.schedule = new TreeMap<>();
    }

    public Human(String firstName, String secondName, String dateOfBirth){
        this.firstName = firstName;
        this.secondName = secondName;
        this.dateOfBirth = LocalDate.parse(dateOfBirth,DateTimeFormatter.ofPattern("dd/MM/yyyy")).atStartOfDay(ZoneId.of("GMT")).toInstant().toEpochMilli();
        this.schedule = new TreeMap<>();
    }

    public Human(String firstName, String secondName, String dateOfBirth, int iq){
        this.firstName = firstName;
        this.secondName = secondName;
        this.dateOfBirth = LocalDate.parse(dateOfBirth,DateTimeFormatter.ofPattern("dd/MM/yyyy")).atStartOfDay(ZoneId.of("GMT")).toInstant().toEpochMilli();
        this.schedule = new TreeMap<>();
        if (iq >= 0 && iq <= 100){
            this.iq = iq;
        } else if (iq > 100) {
            this.iq = 100;
        } else {
            this.iq = 0;
        }
    }
    public Human(String firstName, String secondName, String dateOfBirth, int iq, Map<DayOfWeek, String> schedule) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.dateOfBirth = LocalDate.parse(dateOfBirth,DateTimeFormatter.ofPattern("dd/MM/yyyy")).atStartOfDay(ZoneId.of("GMT")).toInstant().toEpochMilli();
        this.schedule = new TreeMap<>(schedule);

        if (iq >= 0 && iq <= 100){
            this.iq = iq;
        } else if (iq > 100) {
            this.iq = 100;
        } else {
            this.iq = 0;
        }
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public int getYearOfBirth() {
        return Instant.ofEpochMilli(this.dateOfBirth).atZone(ZoneId.of("GMT")).toLocalDate().getYear();
    }

    public String describeAge(){
        return Instant.ofEpochMilli(this.dateOfBirth)
                .atZone(ZoneId.of("GMT"))
                .toLocalDate()
                .format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
    }

    public int getIq() {
        return iq;
    }

    public Map<DayOfWeek, String> getSchedule() {
        return schedule;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public void setYearOfBirth(String dateOfBirth) {
        this.dateOfBirth = LocalDate.parse(dateOfBirth,DateTimeFormatter.ofPattern("dd/MM/yyyy")).atStartOfDay(ZoneId.of("GMT")).toInstant().toEpochMilli();;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public void setSchedule(Map<DayOfWeek, String> schedule) {
        this.schedule = new TreeMap<>(schedule);
    }

    public void greetPets(){
        this.family.getPets().forEach(e -> System.out.printf("Привіт, %s \n", e.getNickname()));
    }

    public void describePet(){
        this.family.getPets().forEach(e -> System.out.printf("У мене є %s, їй %d років, він %s \n",
                e.getNickname(), e.getAge(), e.getTrickLevelByString()));
    }

    public String prettyFormat(){
        return String.format("Human{name='%s', surname='%s', date=%s, iq=%d, schedule=[%s]}",
                getFirstName(),getSecondName(),describeAge(), getIq(), getSchedule().toString());
    }

    @Override
    public String toString() {
        return "Human{" +
                "firstName='" + firstName + '\'' +
                ", secondName='" + secondName + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                ", iq=" + iq +
                ", family=" + family +
                ", schedule=" + schedule +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Human human = (Human) o;

        if (getYearOfBirth() != human.getYearOfBirth()) return false;
        if (getIq() != human.getIq()) return false;
        if (!getFirstName().equals(human.getFirstName())) return false;
        if (!getSecondName().equals(human.getSecondName())) return false;
        if (!getFamily().equals(human.getFamily())) return false;
        return getSchedule().equals(human.getSchedule());
    }

    @Override
    public int hashCode() {
        int result = getFirstName().hashCode();
        result = 31 * result + getSecondName().hashCode();
        result = 31 * result + getYearOfBirth();
        result = 31 * result + getIq();
        result = 31 * result + getFamily().hashCode();
        result = 31 * result + getSchedule().hashCode();
        return result;
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println(toString());
    }
}
