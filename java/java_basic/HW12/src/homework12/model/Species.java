package homework12.model;

import java.io.Serializable;

public enum Species implements Serializable {
    FISH, DOMESTICCAT, DOG, ROBOCAT, UNKNOWN
}
