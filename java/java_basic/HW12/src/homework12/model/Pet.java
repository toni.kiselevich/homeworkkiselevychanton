package homework12.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public abstract class Pet implements Serializable {
    private Species species = Species.UNKNOWN;
    private String nickname;
    private int age;
    private int trickLevel;
    private Set<String> habits;

    public Pet(){
        this.habits = new HashSet<>();
    }

    public Pet(String nickname){
        this.nickname = nickname;
        this.habits = new HashSet<>();
    }
    public Pet(String nickname, int age, int trickLevel, Set<String> habits) {
        this.nickname = nickname;
        this.age = age;
        this.habits = new HashSet<>(habits);
        if (trickLevel >= 0 && trickLevel <= 100){
            this.trickLevel = trickLevel;
        } else if (trickLevel > 100) {
            this.trickLevel = 100;
        } else {
            this.trickLevel = 0;
        }
    }

    public Species getSpecies() {
        return species;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setHabits(Set<String> habits) {
        this.habits = new HashSet<>(habits);;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if (age > this.age){
            this.age = age;
        }
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public String getTrickLevelByString() {
        return this.trickLevel > 50 ? "дуже хитрий" : "майже не хитрий";
    }

    public void setTrickLevel(int trickLevel) {
        if (trickLevel >= 0 && trickLevel <= 100){
            this.trickLevel = trickLevel;
        } else if (trickLevel > 100) {
            this.trickLevel = 100;
        } else {
            this.trickLevel = 0;
        }
    }

    public Set<String> getHabits() {
        return habits;
    }

    public void eat(){
        System.out.println("Я ї'м!");
    }

    public abstract void respond();

    public String prettyFormat () {
        return String.format("%s{nickname=%s, age=%d, trickLevel=%d, habits=[%s]}",
                getSpecies(),getNickname(),getAge(), getTrickLevel(), getHabits().toString());
    }

    @Override
    public String toString() {
        return "Pet{" +
                "species=" + species +
                ", nickname='" + nickname + '\'' +
                ", age=" + age +
                ", trickLevel=" + trickLevel +
                ", habits=" + habits +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Pet pet = (Pet) o;

        if (getAge() != pet.getAge()) return false;
        if (!getSpecies().equals(pet.getSpecies())) return false;
        return getNickname().equals(pet.getNickname());
    }


    @Override
    public int hashCode() {
        int result = getSpecies().hashCode();
        result = 31 * result + getNickname().hashCode();
        result = 31 * result + getAge();
        result = 31 * result + getTrickLevel();
        result = 31 * result + getHabits().hashCode();
        return result;
    }

}
