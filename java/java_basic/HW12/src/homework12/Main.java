package homework12;

import homework12.controller.FamilyController;
import homework12.dao.FileFamilyDao;
import homework12.dao.FamilyDao;
import homework12.service.FamilyService;

public class Main {
    public static void main(String[] args) {
        FamilyDao familyDao = new FileFamilyDao();
        FamilyService familyService = new FamilyService(familyDao);
        FamilyController familyController = new FamilyController(familyService);

        familyController.menu();
    }
}
