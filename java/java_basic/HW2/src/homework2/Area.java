package homework2;


import java.util.Random;
import java.util.Scanner;

public class Area {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String[][] area = new String[6][6];

        for (int i = 0; i < area.length; i++) {
            area[i][0] = area[0][i] = Integer.toString(i);

            for (int j = 1; j < area.length; j++) {
                if (i == 0) {
                    continue;
                }
                area[i][j] = Integer.toString(0);
            }
        }

        Random random = new Random();
        int targetColumn = random.nextInt(5) + 1;
        int targetStartPointLine = random.nextInt(3) + 1;
        int targetEndPointLine = targetStartPointLine + 2;

        int hitCounter = 0;

        System.out.printf("Target line start from %d to %d, target column is %d \n", targetStartPointLine, targetEndPointLine, targetColumn);
        System.out.println("All set. Get ready to rumble!");

        while (true) {
            System.out.println("Please, enter a number of line in this range - [1-5]");

            int selectedLine = scanner.nextInt();
            while (selectedLine < 1 || selectedLine > 5) {
                System.out.println("Only [1-5] range permitted");
                selectedLine = scanner.nextInt();
            }


            System.out.println("Please, enter a number of column in this range - [1-5]");

            int selectedColumn = scanner.nextInt();
            while (selectedColumn < 1 || selectedColumn > 5) {
                System.out.println("Only [1-5] range permitted");
                selectedColumn = scanner.nextInt();
            }


            if (targetColumn == selectedColumn && (selectedColumn >= targetStartPointLine && selectedColumn <= targetEndPointLine)) {
                if (!area[selectedLine][selectedColumn].equals("x")){
                    area[selectedLine][selectedColumn] = "x";
                    hitCounter++;
                } else {
                    System.out.println("You already hit here");
                }

                if (hitCounter == 3) {
                    System.out.println("You have won!");
                    printArea(area);
                    break;
                }
                printArea(area);
            } else {
                area[selectedLine][selectedColumn] = "*";
                printArea(area);
            }
        }
    }

    public static void printArea(String[][] area) {
        for (String[] subline : area) {

            for (int i = 0; i < subline.length; i++) {
                System.out.print(subline[i]);
                if (i < subline.length - 1) {
                    System.out.print(" | ");
                }
            }
            System.out.println();
        }
    }
}
