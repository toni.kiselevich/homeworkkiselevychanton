package home_work1;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Numbers {
    public static void main(String[] args) {
        String[][] MOCK_MATRIX = {
                {"1996", "When was Java 1.0 released?"},
                {"1998", "When was the Java 2 platform introduced?"},
                {"2004", "When was Java 5 released?"},
                {"2011", "When was Java Development Kit (JDK) 7 introduced?"},
                {"2014", "When was Java 8 (Java SE 8) released?"},
                {"2010", "When did Oracle acquire Sun Microsystems, the creator of Java?"},
                {"2006", "When was OpenJDK launched?"},
                {"2017", "When was Java 9 released?"},
                {"2018", "When was Java 11 (Java SE 11) introduced with long-term support (LTS) and JDK modules?"},
                {"2021", "When was Java 16 (Java SE 16) released?"},
        };

        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter your name, please: ");
        String name = scanner.nextLine();

        System.out.println("Let the game begin!");

        Random random = new Random();
        int[] allAnswers = {};
        int answer;
        while (true){
            int[] tempArr = Arrays.copyOf(allAnswers, allAnswers.length + 1);

            int year = random.nextInt(10);

            System.out.println(MOCK_MATRIX[year][1]);

            while (!scanner.hasNextInt()){
                scanner.next();
                System.out.println("You MUST enter a number: ");
            }

            answer = scanner.nextInt();

            tempArr[tempArr.length - 1] = answer;

            allAnswers = tempArr;

            if (answer == Integer.parseInt(MOCK_MATRIX[year][0])){
                System.out.printf("Congratulations, %s ! \n", name);
                System.out.println("Your numbers: " + Arrays.toString(allAnswers));
                System.exit(0);
            }
        }
    }
}
