package test.java.homework5;

import homework6.DayOfWeek;
import homework6.Man;
import homework6.Woman;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class WomanTest {
    @Test
    void testWomanToStringEmptyConstructor() {
        assertEquals("Woman{name='null', surname='null', year=0, iq=0, schedule=[null]",
                new Woman().toString());
    }
    @Test
    void testWomanToStringPartlyFilledConstructor() {
        assertEquals("Woman{name='Olga', surname='Afonasyeva', year=1985, iq=0, schedule=[null]",
                new Woman("Olga", "Afonasyeva", 1985).toString());
    }
    @Test
    void testWomanToStringFullyFilledConstructor() {
        assertEquals("Woman{name='Olga', surname='Afonasyeva', year=1985, iq=100, schedule=[[[MONDAY, BBQ], [TUESDAY, GYM], [WEDNESDAY, DANCE], [THURSDAY, VOCAL], [FRIDAY, SWIM], [SATURDAY, BASKETBALL], [SUNDAY, CINEMA]]]",
                new Woman("Olga",
                        "Afonasyeva",
                        1985,
                        100,
                        new String[][]{{DayOfWeek.MONDAY.name(), "BBQ"},{
                                DayOfWeek.TUESDAY.name(), "GYM"
                        },{
                                DayOfWeek.WEDNESDAY.name(), "DANCE"
                        },{
                                DayOfWeek.THURSDAY.name(), "VOCAL"
                        },{
                                DayOfWeek.FRIDAY.name(), "SWIM"
                        },{
                                DayOfWeek.SATURDAY.name(), "BASKETBALL"
                        },{
                                DayOfWeek.SUNDAY.name(), "CINEMA"
                        }}
                ).toString());
    }
}
