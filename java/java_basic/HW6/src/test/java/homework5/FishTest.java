package test.java.homework5;

import homework6.Fish;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FishTest {
    @Test
    void testFishToStringEmptyConstructor(){
        assertEquals("FISH{nickname=null, age=0, trickLevel=0, habits=[null]}", new Fish().toString());
    }
    @Test
    void testFishToStringPartlyFilledConstructor() {
        assertEquals("FISH{nickname=Bella, age=0, trickLevel=0, habits=[null]}", new Fish("Bella").toString());
    }
    @Test
    void testFishToStringFullyFilledConstructor() {
        assertEquals("FISH{nickname=Bella, age=5, trickLevel=100, habits=[[Hunt, Jump]]}",
                new Fish(
                        "Bella",
                        5,
                        190,
                        new String[]{"Hunt", "Jump"}
                ).toString());
    }
}
