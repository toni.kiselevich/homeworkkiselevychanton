package test.java.homework5;

import static org.junit.jupiter.api.Assertions.assertEquals;

import homework6.Dog;
import org.junit.jupiter.api.Test;

class DogTest {
    @Test
    void testDogToStringEmptyConstructor(){
        assertEquals("DOG{nickname=null, age=0, trickLevel=0, habits=[null]}", new Dog().toString());
    }
    @Test
    void testDogToStringPartlyFilledConstructor() {
        assertEquals("DOG{nickname=Bella, age=0, trickLevel=0, habits=[null]}", new Dog("Bella").toString());
    }
    @Test
    void testDogToStringFullyFilledConstructor() {
        assertEquals("DOG{nickname=Bella, age=5, trickLevel=100, habits=[[Hunt, Jump]]}",
                new Dog(
                        "Bella",
                        5,
                        190,
                        new String[]{"Hunt", "Jump"}
                        ).toString());
    }
}

