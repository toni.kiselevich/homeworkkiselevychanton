package test.java.homework5;


import homework6.DayOfWeek;
import homework6.Human;
import homework6.Man;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ManTest {
    @Test
    void testManToStringEmptyConstructor() {
        assertEquals("Man{name='null', surname='null', year=0, iq=0, schedule=[null]",
                new Man().toString());
    }
    @Test
    void testManToStringPartlyFilledConstructor() {
        assertEquals("Man{name='Kiril', surname='Budanov', year=1985, iq=0, schedule=[null]",
                new Man("Kiril", "Budanov", 1985).toString());
    }
    @Test
    void testManToStringFullyFilledConstructor() {
        assertEquals("Man{name='Kiril', surname='Budanov', year=1985, iq=100, schedule=[[[MONDAY, BBQ], [TUESDAY, GYM], [WEDNESDAY, DANCE], [THURSDAY, VOCAL], [FRIDAY, SWIM], [SATURDAY, BASKETBALL], [SUNDAY, CINEMA]]]",
                new Man("Kiril",
                        "Budanov",
                        1985,
                        100,
                        new String[][]{{DayOfWeek.MONDAY.name(), "BBQ"},{
                                DayOfWeek.TUESDAY.name(), "GYM"
                        },{
                                DayOfWeek.WEDNESDAY.name(), "DANCE"
                        },{
                                DayOfWeek.THURSDAY.name(), "VOCAL"
                        },{
                                DayOfWeek.FRIDAY.name(), "SWIM"
                        },{
                                DayOfWeek.SATURDAY.name(), "BASKETBALL"
                        },{
                                DayOfWeek.SUNDAY.name(), "CINEMA"
                        }}
                ).toString());
    }
}
