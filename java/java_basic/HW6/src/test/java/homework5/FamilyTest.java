package test.java.homework5;

import homework6.Family;
import homework6.Human;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class FamilyTest {

    @Test
    void testAddChild() {
        Human mother = new Human("Jane", "Karoloe", 1994);
        Human father = new Human("Fabrik", "Alfred", 1990);

        Family family = new Family(mother, father);
        Human child = new Human("Jane", "Second Name", 2002);

        family.addChild(child);

        assertEquals(1, family.getChildren().length);
        assertTrue(family.getChildren()[0].equals(child));
    }

    @Test
    void testDeleteChildException() {
        Human mother = new Human("Jane", "Karoloe", 1994);
        Human father = new Human("Fabrik", "Alfred", 1990);

        Family family = new Family(mother, father);

        assertThrows(NegativeArraySizeException.class,
                () -> family.deleteChild(1));
    }


    @Test
    void testCountFamilyWithoutChildren() {
        Human mother = new Human("Jane", "Karoloe", 1994);
        Human father = new Human("Fabrik", "Alfred", 1990);

        Family family = new Family(mother, father);
        assertEquals(2, family.countFamily());
    }

    @Test
    void testCountFamilyWithChildren() {
        Human mother = new Human("Jane", "Karoloe", 1994);
        Human father = new Human("Fabrik", "Alfred", 1990);

        Human child1 = new Human("Jane", "Chrises", 2020);
        Human child2 = new Human("Able", "Culdov", 2020);

        Family family = new Family(mother, father);
        family.addChild(child1);
        family.addChild(child2);

        assertEquals(4, family.countFamily());
    }


    @Test
    void testDeleteRealChild() {

        Human mother = new Human("Jane", "Karoloe", 1994);
        Human father = new Human("Fabrik", "Alfred", 1990);

        Family family = new Family(mother, father);
        Human child = new Human("Jane", "Chrises", 2020);

        family.addChild(child);

        assertTrue(family.deleteChild(0));
    }

    @Test
    void testToStringFamily() {
        Human mother = new Human("Jane", "Karoloe", 1994);
        Human father = new Human("Fabrik", "Alfred", 1990);
        assertEquals(
                "Family{mother=Human{name='Jane', surname='Alfred', year=1994, iq=0, schedule=[null], father=Human"
                        + "{name='Fabrik', surname='Alfred', year=1990, iq=0, schedule=[null], children=[], pet=null}",
                (new Family(mother, father)).toString());
    }

    ;


}

