package test.java.homework5;

import homework6.Dog;
import homework6.DomesticCat;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DomesticCatTest {
    @Test
    void testDomesticCatToStringEmptyConstructor(){
        assertEquals("DOMESTICCAT{nickname=null, age=0, trickLevel=0, habits=[null]}", new DomesticCat().toString());
    }
    @Test
    void testDomesticCatToStringPartlyFilledConstructor() {
        assertEquals("DOMESTICCAT{nickname=Bella, age=0, trickLevel=0, habits=[null]}", new DomesticCat("Bella").toString());
    }
    @Test
    void testDomesticCatToStringFullyFilledConstructor() {
        assertEquals("DOMESTICCAT{nickname=Bella, age=5, trickLevel=100, habits=[[Hunt, Jump]]}",
                new DomesticCat(
                        "Bella",
                        5,
                        190,
                        new String[]{"Hunt", "Jump"}
                ).toString());
    }
}
