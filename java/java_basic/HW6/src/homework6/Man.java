package homework6;

import java.util.Arrays;

public final class Man extends Human {
    public Man() {
    }

    public Man(String firstName, String secondName, int yearOfBirth) {
        super(firstName, secondName, yearOfBirth);
    }

    public Man(String firstName, String secondName, int yearOfBirth, int iq, String[][] schedule) {
        super(firstName, secondName, yearOfBirth, iq, schedule);
    }

    @Override
    public String toString() {
        return String.format("Man{name='%s', surname='%s', year=%d, iq=%d, schedule=[%s]",
                getFirstName(),getSecondName(),getYearOfBirth(), getIq(), Arrays.deepToString(getSchedule()));
    }
    @Override
    public void greetPet() {
        System.out.printf("Привіт, %s. Батько дома, принеси ка мені капці. \n", getFamily().getPet().getNickname());
    }

    public void repairCar(){
        System.out.println("Прийшов час полагодити наше авто. Я пішов в гараж.");
    }
}
