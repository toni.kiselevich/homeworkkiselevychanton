package homework6;

import java.util.Arrays;

public class Family {
    private Human mother;
    private Human father;
    private Human[] children;
    private Pet pet;

    public Family(Human mother, Human father){
        mother.setFamily(this);
        mother.setSecondName(father.getSecondName());
        this.mother = mother;
        father.setFamily(this);
        this.father = father;

        this.children = new Human[]{};
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public Human[] getChildren() {
        return children;
    }

    public void setChildren(Human[] children) {
        this.children = children;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public void addChild(Human child){
        child.setFamily(this);
        child.setSecondName(this.father.getSecondName());

        Human[] children = Arrays.copyOf(this.children, this.children.length + 1);
        children[children.length - 1] = child;

        setChildren(children);
    }

    public boolean deleteChild(int indexOfChild){
        Human[] children = new Human[this.children.length - 1];
        boolean isChildFound = false;

        for (int i = 0; i < this.children.length; i++) {
            if (i == indexOfChild){
                isChildFound = true;
                continue;
            }
            children[i] = this.children[i];
        }

        if (isChildFound){
            setChildren(children);
        }

        return isChildFound;
    }

    public int countFamily(){
        return 2 + this.children.length;
    }


    @Override
    public String toString() {
        return "Family{" +
                "mother=" + mother +
                ", father=" + father +
                ", children=" + Arrays.toString(children) +
                ", pet=" + pet +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Family family = (Family) o;

        if (!getMother().equals(family.getMother())) return false;
        if (!getFather().equals(family.getFather())) return false;

        if (!Arrays.equals(getChildren(), family.getChildren())) return false;
        return getPet().equals(family.getPet());
    }

    @Override
    public int hashCode() {
        int result = getMother().hashCode();
        result = 31 * result + getFather().hashCode();
        result = 31 * result + Arrays.hashCode(getChildren());
        result = 31 * result + getPet().hashCode();
        return result;
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println(toString());
    }
}
