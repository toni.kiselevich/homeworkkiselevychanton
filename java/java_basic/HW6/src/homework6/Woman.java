package homework6;

import java.util.Arrays;

public final class Woman extends Human{
    public Woman() {
    }

    public Woman(String firstName, String secondName, int yearOfBirth) {
        super(firstName, secondName, yearOfBirth);
    }

    public Woman(String firstName, String secondName, int yearOfBirth, int iq, String[][] schedule) {
        super(firstName, secondName, yearOfBirth, iq, schedule);
    }

    @Override
    public String toString() {
        return String.format("Woman{name='%s', surname='%s', year=%d, iq=%d, schedule=[%s]",
                getFirstName(),getSecondName(),getYearOfBirth(), getIq(), Arrays.deepToString(getSchedule()));
    }
    @Override
    public void greetPet() {
        System.out.printf("Привіт, %s. Мати дома, пішли на кухню погодую тебе. \n", getFamily().getPet().getNickname());
    }

    public void makeup(){
        System.out.println("Я майже готова, залишилось нанести помаду на губи.");
    }
}
