package test.java.homework11;

import homework11.model.DayOfWeek;
import homework11.model.Woman;
import org.junit.jupiter.api.Test;

import java.util.Map;
import java.util.TreeMap;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class WomanTest {
    @Test
    void testWomanToStringEmptyConstructor() {
        assertEquals("Woman{name='null', surname='null', date=01/01/1970, iq=0, schedule=[{}]}",
                new Woman().toString());
    }
    @Test
    void testWomanToStringPartlyFilledConstructor() {
        assertEquals("Woman{name='Olga', surname='Afonasyeva', date=11/08/1985, iq=0, schedule=[{}]}",
                new Woman("Olga", "Afonasyeva", "11/08/1985").toString());
    }
    @Test
    void testWomanToStringFullyFilledConstructor() {
        Map<DayOfWeek, String> schedule = new TreeMap<>();
        schedule.put(DayOfWeek.MONDAY, "BBQ");
        schedule.put(DayOfWeek.TUESDAY, "GYM");
        schedule.put(DayOfWeek.WEDNESDAY, "DANCE");
        schedule.put(DayOfWeek.THURSDAY, "VOCAL");
        schedule.put(DayOfWeek.FRIDAY, "SWIM");
        schedule.put(DayOfWeek.SATURDAY, "BASKETBALL");
        schedule.put(DayOfWeek.SUNDAY, "CINEMA");

        assertEquals("Woman{name='Olga', surname='Afonasyeva', date=11/08/1985, iq=100, schedule=[{MONDAY=BBQ, TUESDAY=GYM, WEDNESDAY=DANCE, THURSDAY=VOCAL, FRIDAY=SWIM, SATURDAY=BASKETBALL, SUNDAY=CINEMA}]}",
                new Woman("Olga",
                        "Afonasyeva",
                        "11/08/1985",
                        100,
                        schedule
                ).toString());
    }
}
