package test.java.homework11;

import homework11.model.Family;
import homework11.model.Human;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FamilyTest {

    @Test
    void testConstructor() {
        Human mother = new Human("Jane", "Second Name", "11/08/1999");

        Human father = new Human("Jane", "Second Name", "11/08/1999");

        Family actualFamily = new Family(mother, father);

        assertTrue(actualFamily.getChildren().isEmpty());
        Human mother2 = actualFamily.getMother();
        assertSame(mother, mother2);
        Human father2 = actualFamily.getFather();
        assertSame(father, father2);
        assertSame(actualFamily, father2.getFamily());
        assertEquals("Second Name", mother2.getSecondName());
        assertSame(actualFamily, mother2.getFamily());
    }

    @Test
    void deleteChildByObjWhenChildIsPresent() {
        Human mother = new Human("Jane", "Karoloe", "11/08/1994");
        Human father = new Human("Fabrik", "Alfred", "11/08/1990");
        Family family = new Family(mother, father);

        Human child1 = new Human("Child1", "Doe", "11/08/2005");
        Human child2 = new Human("Child2", "Doe", "11/08/2010");
        family.addChild(child1);
        family.addChild(child2);

        assertTrue(family.deleteChild(child1));
        assertFalse(family.getChildren().contains(child1));
    }

    @Test
    void deleteChildByObjWhenChildIsNotPresent() {
        Human mother = new Human("Jane", "Karoloe", "11/08/1994");
        Human father = new Human("Fabrik", "Alfred", "11/08/1990");
        Family family = new Family(mother, father);

        Human child1 = new Human("Child1", "Doe", "11/08/2005");
        Human child2 = new Human("Child2", "Doe", "11/08/2010");
        family.addChild(child1);
        family.addChild(child2);

        Human childToRemove = new Human("Child3", "Doe", "11/08/2015");

        assertFalse(family.deleteChild(childToRemove));
        assertEquals(2, family.getChildren().size());
    }

    @Test
    void deleteChildAtIndex() {
        Human mother = new Human("Jane", "Karoloe", "11/08/1994");
        Human father = new Human("Fabrik", "Alfred", "11/08/1990");

        Family family = new Family(mother, father);
        family.addChild(new Human("Flora", "Korile", "11/08/2019"));
        family.addChild(new Human("Alda", "Flavo", "11/08/2020"));

        int initialSize = family.getChildren().size();
        int indexOfChildToDelete = 1;

        Human willBeRemovedChild = family.getChildren().get(indexOfChildToDelete);

        Human finallyRemovedChild = family.deleteChild(indexOfChildToDelete);

        assertEquals(initialSize - 1, family.getChildren().size());
        assertEquals(willBeRemovedChild, finallyRemovedChild);
    }

    @Test
    void testDeleteChildException() {
        Human mother = new Human("Jane", "Karoloe", "11/08/1994");
        Human father = new Human("Fabrik", "Alfred", "11/08/1990");

        Family family = new Family(mother, father);

        assertThrows(IndexOutOfBoundsException.class,
                () -> family.deleteChild(1));
    }

    @Test
    void testCountFamilyWithoutChildren() {
        Human mother = new Human("Jane", "Karoloe", "11/08/1994");
        Human father = new Human("Fabrik", "Alfred", "11/08/1990");

        Family family = new Family(mother, father);
        assertEquals(2, family.countFamily());
    }

    @Test
    void testCountFamilyWithChildren() {
        Human mother = new Human("Jane", "Karoloe", "11/08/1994");
        Human father = new Human("Fabrik", "Alfred", "11/08/1990");

        Human child1 = new Human("Jane", "Chrises", "11/08/2020");
        Human child2 = new Human("Able", "Culdov", "11/08/2020");

        Family family = new Family(mother, father);
        family.addChild(child1);
        family.addChild(child2);

        assertEquals(4, family.countFamily());
    }

    @Test
    void testToStringFamily() {
        Human mother = new Human("Jane", "Karoloe", "11/08/1994");
        Human father = new Human("Fabrik", "Alfred", "11/08/1990");
        assertEquals(
                "Family{mother=Human{name='Jane', surname='Alfred', date=11/08/1994, iq=0, schedule=[{}]}, father=Human"
                        + "{name='Fabrik', surname='Alfred', date=11/08/1990, iq=0, schedule=[{}]}, children=[], pet=[]}",
                (new Family(mother, father)).toString());
    }
}

