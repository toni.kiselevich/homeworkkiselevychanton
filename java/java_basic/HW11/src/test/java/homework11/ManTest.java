package test.java.homework11;


import homework11.model.DayOfWeek;
import homework11.model.Man;
import org.junit.jupiter.api.Test;

import java.util.Map;
import java.util.TreeMap;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ManTest {
    @Test
    void testManToStringEmptyConstructor() {
        assertEquals("Man{name='null', surname='null', date=01/01/1970, iq=0, schedule=[{}]}",
                new Man().toString());
    }
    @Test
    void testManToStringPartlyFilledConstructor() {
        assertEquals("Man{name='Kiril', surname='Budanov', date=11/08/1985, iq=0, schedule=[{}]}",
                new Man("Kiril", "Budanov", "11/08/1985").toString());
    }
    @Test
    void testManToStringFullyFilledConstructor() {
        Map<DayOfWeek, String> schedule = new TreeMap<>();
        schedule.put(DayOfWeek.MONDAY, "BBQ");
        schedule.put(DayOfWeek.TUESDAY, "GYM");
        schedule.put(DayOfWeek.WEDNESDAY, "DANCE");
        schedule.put(DayOfWeek.THURSDAY, "VOCAL");
        schedule.put(DayOfWeek.FRIDAY, "SWIM");
        schedule.put(DayOfWeek.SATURDAY, "BASKETBALL");
        schedule.put(DayOfWeek.SUNDAY, "CINEMA");

        assertEquals("Man{name='Kiril', surname='Budanov', date=11/08/1985, iq=100, schedule=[{MONDAY=BBQ, TUESDAY=GYM, WEDNESDAY=DANCE, THURSDAY=VOCAL, FRIDAY=SWIM, SATURDAY=BASKETBALL, SUNDAY=CINEMA}]}",
                new Man("Kiril",
                        "Budanov",
                        "11/08/1985",
                        100,
                        schedule
                ).toString());
    }
}
