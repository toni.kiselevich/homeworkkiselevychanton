package homework11.exceptions;

public class FamilyOverflowException extends RuntimeException {
    public FamilyOverflowException() {
    }

    public FamilyOverflowException(String message) {
        super(message);
    }

    public FamilyOverflowException(String message, Throwable cause) {
        super(message, cause);
    }
}
