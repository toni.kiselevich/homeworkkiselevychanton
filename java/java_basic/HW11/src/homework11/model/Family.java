package homework11.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Family {
    private Human mother;
    private Human father;
    private List<Human> children;
    private Set<Pet> pets;

    public Family(Human mother, Human father) {
        mother.setFamily(this);
        mother.setSecondName(father.getSecondName());
        this.mother = mother;
        father.setFamily(this);
        this.father = father;

        this.children = new ArrayList<>();
        this.pets = new HashSet<>();
    }

    public Human getMother() {
        return mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public List<Human> getChildren() {
        return children;
    }

    public void setChildren(List<Human> children) {
        this.children = new ArrayList<>(children);
    }

    public Set<Pet> getPets() {
        return pets;
    }

    public void setPets(Set<Pet> pets) {
        this.pets = pets;
    }

    public void addPet(Pet pet) {
        this.pets.add(pet);
    }

    public void addChild(Human child) {
        child.setFamily(this);
        child.setSecondName(this.father.getSecondName());

        this.children.add(child);
    }

    public Human deleteChild(int indexOfChild) {

        return this.children.remove(indexOfChild);
    }

    public boolean deleteChild(Human child) {
        return this.children.remove(child);
    }

    public int countFamily() {
        return 2 + this.children.size();
    }

    public String prettyFormat() {
        StringBuilder children = new StringBuilder();
        for (Human child : getChildren()) {
            children.append("       ").append(child.prettyFormat()).append("\n");
        }

        return String.format("family:%n   mother: %s,%n   father: %s,%n   children:%n%s   pets: %s",
                getMother().prettyFormat(), getFather().prettyFormat(), children, getPets());
    }

    @Override
    public String toString() {
        StringBuilder children = new StringBuilder();
        for (Human child : getChildren()) {
            children.append("       ").append(child.getFirstName()).append(" ").append(child.getSecondName()).append("\n");
        }

        return "Family{" +
                "mother=" + mother.getFirstName() + " " + mother.getSecondName() +
                ", father=" + father.getFirstName() + " " + father.getSecondName() +
                ", children=\n" + children +
                ", pets=" + pets +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Family family = (Family) o;

        if (!getMother().equals(family.getMother())) return false;
        return getFather().equals(family.getFather());
    }

    @Override
    public int hashCode() {
        int result = getMother().hashCode();
        result = 31 * result + getFather().hashCode();
        result = 31 * result + getChildren().hashCode();
        result = 31 * result + getPets().hashCode();
        return result;
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println(toString());
    }
}
