package homework11.model;

public enum Species {
    FISH, DOMESTICCAT, DOG, ROBOCAT, UNKNOWN
}
