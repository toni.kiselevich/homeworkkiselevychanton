package homework11.model;

import java.util.Map;

public final class Woman extends Human{

    public Woman() {
    }

    public Woman(String firstName, String dateOfBirth) {
        super(firstName, dateOfBirth);
    }

    public Woman(String firstName, String secondName, String dateOfBirth) {
        super(firstName, secondName, dateOfBirth);
    }

    public Woman(String firstName, String secondName, String dateOfBirth, int iq) {
        super(firstName, secondName, dateOfBirth, iq);
    }

    public Woman(String firstName, String secondName, String dateOfBirth, int iq, Map<DayOfWeek, String> schedule) {
        super(firstName, secondName, dateOfBirth, iq, schedule);
    }

    @Override
    public String prettyFormat(){
        return String.format("Woman{name='%s', surname='%s', date=%s, iq=%d, schedule=[%s]}",
                getFirstName(),getSecondName(),describeAge(), getIq(), getSchedule().toString());
    }

    @Override
    public String toString() {
        return "Woman{" +
                "firstName='" + getFirstName() + '\'' +
                ", secondName='" + getSecondName() + '\'' +
                ", dateOfBirth=" + describeAge() +
                ", iq=" + getIq() +
                ", family=" + getFamily() +
                ", schedule=" + getSchedule() +
                '}';
    }
    @Override
    public void greetPets() {
        getFamily().getPets().forEach(e -> System.out.printf("Привіт, %s. Мати дома, пішли на кухню погодую тебе. \n",
                e.getNickname()));
    }

    public void makeup(){
        System.out.println("Я майже готова, залишилось нанести помаду на губи.");
    }
}
