package homework11.service;

import homework11.dao.FamilyDao;
import homework11.model.*;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class FamilyService {
    private final FamilyDao familyDao;

    public FamilyService(FamilyDao familyDao) {
        this.familyDao = familyDao;
    }

    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }

    public void createMockFamilies(){
        Family firstFamily = new Family(
                new Woman("Alina", "Berezovych", "15/04/1994", 120),
                new Man("Kiril", "Lobov", "11/10/1991", 190)
        );
        firstFamily.addChild(new Woman("Lesya", "19/01/2002"));
        firstFamily.addChild(new Woman("Vasilisa", "02/11/2018"));
        familyDao.saveFamily(firstFamily);

        Family secondFamily = new Family(
                new Woman("Vlada", "Avgustinova", "05/12/1999", 120),
                new Man("Alex", "Rabski", "12/11/2002", 190)
        );
        secondFamily.addChild(new Man("Jora", "02/11/2022"));
        familyDao.saveFamily(secondFamily);
    }
    public void displayAllFamilies() {
        getAllFamilies().forEach(family -> System.out.println(family.prettyFormat() + "\n"));
    }

    public void getFamiliesBiggerThan(int quantity) {
        familyDao.getAllFamilies().stream()
                .filter(family -> family.countFamily() > quantity)
                .forEach(family -> System.out.println(family.prettyFormat() + "\n"));
    }

    public void getFamiliesLessThan(int quantity) {
        familyDao.getAllFamilies().stream()
                .filter(family -> family.countFamily() < quantity)
                .forEach(family -> System.out.println(family.prettyFormat() + "\n"));
    }

    public void countFamiliesWithMemberNumber(int quantity) {
        familyDao.getAllFamilies().stream()
                .filter(family -> family.countFamily() == quantity)
                .forEach(family -> System.out.println(family.prettyFormat() + "\n"));
    }

    public void createNewFamily(Woman mother, Man father) {
        familyDao.saveFamily(new Family(mother, father));
    }

    public boolean deleteFamilyByIndex(int index) {
        return familyDao.deleteFamily(index);
    }

    public Family bornChild(Family family, String maleName, String femaleName, boolean isMale) {
        family.addChild(
                isMale
                        ?
                        new Man(maleName, LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")))
                        :
                        new Woman(femaleName, LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"))));
        familyDao.saveFamily(family);
        return family;
    }

    public Family adoptChild(Family family, Human child) {
        family.addChild(child);
        familyDao.saveFamily(family);
        return family;
    }

    public void deleteAllChildrenOlderThen(int age) {
        familyDao.getAllFamilies().
                forEach(family -> {
                    List<Human> children = family.getChildren()
                            .stream()
                            .filter(child -> Period.between(LocalDate.ofYearDay(child.getYearOfBirth(), 1), LocalDate.now()).getYears() < age)
                            .collect(Collectors.toList());

                    family.setChildren(children);
                });
    }

    public int count() {
        return familyDao.getAllFamilies().size();
    }

    public Family getFamilyById(int index) {
        return familyDao.getFamilyByIndex(index);
    }

    public Set<Pet> getPets(int index) {
        return familyDao.getFamilyByIndex(index).getPets();
    }

    public void addPet(int index, Pet newPet) {
        familyDao.getFamilyByIndex(index).addPet(newPet);
    }
}

