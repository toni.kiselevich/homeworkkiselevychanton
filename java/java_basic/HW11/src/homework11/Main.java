package homework11;

import homework11.controller.FamilyController;
import homework11.dao.CollectionFamilyDao;
import homework11.dao.FamilyDao;
import homework11.model.Dog;
import homework11.model.Human;
import homework11.model.Man;
import homework11.model.Woman;
import homework11.service.FamilyService;

public class Main {
    public static void main(String[] args) {
        FamilyDao familyDao = new CollectionFamilyDao();
        FamilyService familyService = new FamilyService(familyDao);
        FamilyController familyController = new FamilyController(familyService);

        familyController.menu();
    }
}
