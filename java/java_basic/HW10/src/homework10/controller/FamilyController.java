package homework10.controller;

import homework10.model.*;
import homework10.service.FamilyService;

import java.util.List;
import java.util.Set;

public class FamilyController {
    private final FamilyService familyService;

    public FamilyController(FamilyService familyService) {
        this.familyService = familyService;
    }

    public List<Family> getAllFamilies() {
        return familyService.getAllFamilies();
    }

    public void displayAllFamilies() {
        familyService.displayAllFamilies();
    }

    public void getFamiliesBiggerThan(int quantity) {
        familyService.getFamiliesBiggerThan(quantity);
    }

    public void getFamiliesLessThan(int quantity) {
        familyService.getFamiliesLessThan(quantity);
    }

    public void countFamiliesWithMemberNumber(int quantity) {
        familyService.countFamiliesWithMemberNumber(quantity);
    }

    public void createNewFamily(Woman mother, Man father) {
        familyService.createNewFamily(mother, father);
    }

    public boolean deleteFamilyByIndex(int index) {
        return familyService.deleteFamilyByIndex(index);
    }

    public Family bornChild(Family family, String maleName, String femaleName, boolean isMale) {
        return familyService.bornChild(family, maleName, femaleName, isMale);
    }

    public Family adoptChild(Family family, Human child) {
        return familyService.adoptChild(family, child);
    }

    public void deleteAllChildrenOlderThen(int age) {
        familyService.deleteAllChildrenOlderThen(age);
    }

    public int count() {
        return familyService.count();
    }

    public Family getFamilyById(int index) {
        return familyService.getFamilyById(index);
    }

    public Set<Pet> getPets(int index) {
        return familyService.getPets(index);
    }

    public void addPet(int index, Pet newPet) {
        familyService.addPet(index, newPet);
    }
}
