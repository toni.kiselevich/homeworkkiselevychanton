package homework10.model;

import java.util.Set;

public class RoboCat extends Pet{

    public RoboCat() {
        setSpecies(Species.ROBOCAT);
    }

    public RoboCat(String nickname) {
        super(nickname);
        setSpecies(Species.ROBOCAT);
    }

    public RoboCat(String nickname, int age, int trickLevel, Set<String> habits) {
        super(nickname, age, trickLevel, habits);
        setSpecies(Species.ROBOCAT);
    }

    @Override
    public void respond() {
        System.out.printf("Вітаю, я робокіт ! Моє ім'я - %s. Стан заряду - 100%%. \n", getNickname());
    }

    @Override
    public void eat() {
        System.out.println("Я заряджаюсь !");
    }
}
