package homework10.model;

public enum Species {
    FISH, DOMESTICCAT, DOG, ROBOCAT, UNKNOWN
}
