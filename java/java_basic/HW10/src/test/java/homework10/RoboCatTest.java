package test.java.homework10;

import homework10.model.RoboCat;
import org.junit.jupiter.api.Test;

import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RoboCatTest {
    @Test
    void testDogToStringEmptyConstructor(){
        assertEquals("ROBOCAT{nickname=null, age=0, trickLevel=0, habits=[[]]}", new RoboCat().toString());
    }
    @Test
    void testDogToStringPartlyFilledConstructor() {
        assertEquals("ROBOCAT{nickname=Bella, age=0, trickLevel=0, habits=[[]]}", new RoboCat("Bella").toString());
    }
    @Test
    void testDogToStringFullyFilledConstructor() {
        assertEquals("ROBOCAT{nickname=Bella, age=5, trickLevel=100, habits=[[]]}",
                new RoboCat(
                        "Bella",
                        5,
                        190,
                        new HashSet<>()
                ).toString());
    }
}
