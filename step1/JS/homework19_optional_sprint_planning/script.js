const sprintPlanning = (team,tasks,deadLine) => {
    let totalPointsByDays = 0;
    team.forEach(person => {
        totalPointsByDays += person;
    });

    let totalTasksPrice = 0;
    tasks.forEach(task => {
        totalTasksPrice += task;
    });

    const startDay = new Date();
    const endDay = new Date(deadLine);
    let counterDays = 1;
    while (startDay < endDay){
        counterDays++;
        startDay.setDate(startDay.getDate() + 1);
    }

    if (new Date().getDay() === 5){
        counterDays -= 2;
    } else if (new Date().getDay() === 6){
        counterDays -= 2;
    }

    if (counterDays > 7){
        const weeks = parseInt(counterDays / 7);
        counterDays -= weeks * 2;
    }

    const daysNeeded = totalTasksPrice / totalPointsByDays;


    let daysBeforeDeadLine = counterDays - daysNeeded;
    if (!Number.isInteger(daysBeforeDeadLine)){
        daysBeforeDeadLine = parseInt(daysBeforeDeadLine) + 1;
    }

    if (daysNeeded <= counterDays){
        alert(`Усі завдання будуть успішно виконані за ${daysBeforeDeadLine} днів до настання дедлайну!`);
    } else {
        const hours = (daysNeeded - counterDays)  * 8;
        alert(`Команді розробників доведеться витратити  ${hours} годин після дедлайну, щоб виконати всі завдання в беклозі`)
    }
}

sprintPlanning([2,2,5], [14,1,1], ['July 15, 2022']);
