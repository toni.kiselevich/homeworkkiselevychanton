

const itemLinks = document.querySelectorAll('.item-link');

const swapColors = (elems) => {


    elems.forEach(item => {
        item.classList.toggle('item-link-swapped');
        if (!item.classList.contains('item-link-swapped')){
            localStorage.removeItem('layout');
        } else {
            localStorage.setItem('layout' , 'colorSwapped')
        }
    });

}

const button = document.querySelector('.swap-colors');
button.addEventListener('click', () => {
    swapColors(itemLinks)
});

const themeAfterReload = () => {
    if (localStorage.getItem('layout')) {
        swapColors(itemLinks);
    }
}


themeAfterReload();