const factorial = () => {
    let num = Number(prompt('Enter your number:'));
    while(!Number(num) || num === 0){
        num = Number(prompt('Enter your number:'));
    }

    let result = 1;
    for (let i = 1; i <= num; i++){
        result *= i;
    }
    alert(result);
}
factorial();