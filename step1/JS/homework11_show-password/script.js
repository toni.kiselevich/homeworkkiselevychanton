const iconFn =() => {
    const icons = document.querySelector('i');
    const button = document.querySelector('.btn');

    const iconSwap = () => {
        const icons = document.querySelectorAll('i');
        const inputs = document.querySelectorAll('input');
        icons.forEach(icon => {
            if (icon.classList.contains('fa-eye')){
                icon.classList.replace('fa-eye','fa-eye-slash' );
                inputs.forEach(input => {
                    input.setAttribute('type', 'text');
                });
            } else if (icon.classList.contains('fa-eye-slash')) {
                icon.classList.replace('fa-eye-slash','fa-eye' );
                inputs.forEach(input => {
                    input.setAttribute('type', 'password');
                });
            }
        } )
    }

    const checkPass = () => {
        const firstInput = document.querySelector('.first-input');
        const secondInput = document.querySelector('.second-input');
        if (firstInput.value === secondInput.value){
            document.querySelector('p').classList.replace('warning', 'hide');
            alert('You are welcome');
        } else {
            document.querySelector('p').classList.replace('hide', 'warning');
        }
    }


    icons.addEventListener('click', event => {
        if (event.target.nodeName === 'I') {
            iconSwap();
        }
    })

    button.addEventListener('click', (event) => {
        event.preventDefault();
        checkPass();
    })

}


iconFn();