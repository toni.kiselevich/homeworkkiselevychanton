//Опишіть своїми словами що таке Document Object Model (DOM)
//Це модель яка надає нам всі елементи сторінки як об'єкти для подальшої роботи з ними.


// Яка різниця між властивостями HTML-елементів innerHTML та innerText?
//InnerHTML буде читатися як ХТМЛ код, а InnerText як звичайний текст


// Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
//Я вважаю, що краще всього через querySelector



const paragraphs = document.body.querySelectorAll('p');
paragraphs.forEach(element => {
    element.style.backgroundColor = '#ff0000';
});

const elem1 = document.getElementById('optionsList')
console.log(`елемент із id="optionsList":`);
console.log(elem1)

console.log('///////////////////////////////')

console.log(`батьківський елемент :`);
console.log(elem1.parentNode);


console.log('///////////////////////////////')

const childNodes = elem1.childNodes;
childNodes.forEach(node => {
    console.log(`Дочірній елемент : ${node} типу : ${typeof node}`);
    console.log(node);
    console.log(`
    `);
});


const testParagraph = document.getElementById('testParagraph');
testParagraph.innerText = 'This is a paragraph';

const navItems = document.querySelector('.main-header').children;

for (let navItem of navItems) {
    console.log(navItem)
    navItem.classList.add('nav-item');
}

