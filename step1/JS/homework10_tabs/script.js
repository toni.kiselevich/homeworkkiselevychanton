const tabs = document.querySelectorAll('.tabs-title');

tabs.forEach((tab) => {
    tab.addEventListener('click', () => {

        tabs.forEach((tab) => {
            tab.classList.remove('active');
        });
        if (! tab.classList.contains('active')){
            tab.classList.add('active');
        }




        let tabId = tab.getAttribute('data-tab');
        let currentContent = document.querySelector(tabId);
        let tabsContent = document.querySelectorAll('.tabs-content');

        tabsContent.forEach((content) => {
            content.classList.remove('active');
        })

        if (! currentContent.classList.contains('active')){
            currentContent.classList.add('active');
        }



    })
})