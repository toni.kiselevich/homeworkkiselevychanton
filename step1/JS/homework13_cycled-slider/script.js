let counter = 1;

const swapImg = () => {
    document.body.insertAdjacentHTML('beforeend', '<button class = "stop-button">Припинити</button>');
    const stopBtn = document.body.querySelector('.stop-button');
    document.body.insertAdjacentHTML('beforeend', '<button class = "continue-button" >Відновити показ</button>');
    const contBtn = document.body.querySelector('.continue-button');

    const swapperFn = () => {
        const swapper = setInterval(() => {
            if (counter > 4) {
                counter = 1;
            }
            document.querySelector('.image-to-show').src = `./img/${counter}.jpg`;
            counter++;
        }, 3000);
        stopBtn.addEventListener('click', () => {
            clearInterval(swapper);
        });
    }
    swapperFn()


    contBtn.addEventListener('click', () => {
        swapperFn();
    });
}


swapImg();