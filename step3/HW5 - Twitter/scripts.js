const USERS_URL = 'https://ajax.test-danit.com/api/json/users';
const POSTS_URL = 'https://ajax.test-danit.com/api/json/posts';
const root = document.querySelector('#root');


class Data {
    constructor(USERS_URL, POSTS_URL) {
        this.USERS_URL = USERS_URL;
        this.POSTS_URL = POSTS_URL;
    }
    getUsers() {
        return axios.get(this.USERS_URL);
    }
    getPosts(){
        return axios.get(this.POSTS_URL);
    }
    publishPost(title, body, userId){
        return axios({
            method: 'post',
            url: this.POSTS_URL,
            data: {
                title: title,
                body: body,
                userId: userId,
            }
        });
    }
    delPosts(postId){
        return axios.delete(`${this.POSTS_URL}/${postId}`);
    }
}

const dataRequest = new Data(USERS_URL, POSTS_URL);
dataRequest.getUsers();
dataRequest.getPosts();


class Card {
    constructor(root) {
        this.root = root
    }
    render(publishPost){

        if (publishPost){
            const form = document.createElement('form');
            form.classList.add('publish-post-form');

            const title = document.createElement('input');
            title.type = 'text';
            title.placeholder = 'Title';
            title.id = 'title';
            form.append(title);

            const text = document.createElement('input');
            text.type = 'text';
            text.placeholder = 'Text';
            text.id = 'text';
            form.append(text);

            const submit = document.createElement('button');
            submit.type = 'submit';
            submit.textContent = 'Опубликовать пост';
            submit.id = 'submit';
            form.append(submit);

            document.body.prepend(form)


            submit.addEventListener('click', (e) => {
                e.preventDefault();
                dataRequest.publishPost(title.value, text.value, 1)
                    .then(function (){

                    }, function (){
                        console.log('DID NOT POSTED')
                    })
                form.remove()
            })
        }
        else {
            dataRequest.getPosts()
                .then(({data}) => {
                    data.forEach(({title, body, userId, id : postId}) => {
                        const card = document.createElement('div');
                        card.classList.add('card-wrapper');
                        card.dataset.id = postId;

                        const postTitle = document.createElement('h3');
                        postTitle.textContent = title;
                        card.append(postTitle);

                        const postBody = document.createElement('p');
                        postBody.textContent = body;
                        card.append(postBody);

                        dataRequest.getUsers()
                            .then(({data}) => {
                                data.forEach(({id, name, email}) => {
                                    if (userId === id){
                                        const nameOfUser = document.createElement('h3');
                                        nameOfUser.textContent = name;
                                        card.append(nameOfUser)


                                        const mailOfUser = document.createElement('p');
                                        mailOfUser.textContent = email;
                                        card.append(mailOfUser);

                                        const delBtn = document.createElement('button');
                                        delBtn.type = 'submit';
                                        delBtn.classList.add('del-btn');
                                        delBtn.textContent = 'Удалить'
                                        delBtn.dataset.id = postId;
                                        card.append(delBtn);
                                    }
                                })
                            })
                        this.root.append(card)
                    })
                })
        }


    }
}
const card = new Card(root);
card.render()


root.addEventListener('click', (event) => {
    if (event.target.tagName ===  'BUTTON'){
        let id = event.target.dataset.id;
        dataRequest.delPosts(id)
            .then(function (){
                document.querySelector(`[data-id = "${id}" ]`).remove();
            }, function (){
                console.log('DID NOT DELETED')
            })
    }

})

const addPostBtn = document.querySelector('.add-post-btn');
addPostBtn.addEventListener('click', () => {
    card.render(true)
});

/*const publishPostBtn = document.querySelector('#submit');
publishPostBtn.addEventListener('click', () => {

})*/


