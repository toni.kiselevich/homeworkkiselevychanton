// Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
//У кожного об'єкта є свій прототип, він або невизначений null або зберігає силку на інший об'єкт. Коли ми у нащадка викликаємо метод який у нього невизначений, цей метод береться з його прототипу


// Для чого потрібно викликати super() у конструкторі класу-нащадка?
//Коли ми наслідуємо якийсь клас, ми також наслідєм його конструктор і за допомогою супер ми викликаємо батьківський конструктор


class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name(){
        return this._name;
    }
    set name(name){
        this._name = name;
    }

    get age(){
        return this._age;
    }
    set age(age){
        this._age = age;
    }

    get salary(){
        return this._salary;
    }
    set salary(salary){
        this._salary = salary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
    }

    get salary(){
        return this._salary*3;
    }
}


const anton = new Programmer('Anton', '19', 900, ['Ukrainian', 'Russian', 'English']);
const polina = new Programmer('Polina', '21', 700, ['Spanish', 'Ukrainian']);

console.log(anton);
console.log(polina);

console.log(anton.salary);
console.log(polina.salary);
