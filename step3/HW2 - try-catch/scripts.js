class Book {
    constructor(author, name, price) {
        try {
            if (author !== undefined) {
                this.author = author;
            } else {
                throw new Error('Автор не вказаний');
            }

            if (name !== undefined) {
                this.name = name;
            } else {
                throw new Error("Ім'я не вказане");
            }

            if (price !== undefined) {
                this.price = price;
            } else {
                throw new Error("Ціна не вказана");
            }
        } catch (e) {
            console.log(e)
        }

    }

    render(root) {
        if (!root.hasChildNodes(document.querySelector('.book-list'))){
            const bookList = document.createElement('ul');
            bookList.classList.add('book-list')
            root.append(bookList);
        }

        if (this.author !== undefined && this.name !== undefined && this.price !== undefined) {
            const bookList = document.querySelector('.book-list');
            let bookInfo = document.createElement('li');
            bookInfo.textContent = `${this.author} - ${this.name} - ${this.price}`;
            bookList.append(bookInfo);
            console.log(bookInfo + "LOL")
        }
    }
}


const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

const root = document.querySelector('#root');
books.forEach((book) => {
    let renderBook = new Book(book.author, book.name, book.price);
    renderBook.render(root)
});
