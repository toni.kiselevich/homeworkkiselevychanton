const employee = {
    name: 'Vitalii',
    surname: 'Klichko'
}

const employeeResult = {
    ...employee,
    age: 24,
    salary: 1000
}

console.log(employeeResult)
