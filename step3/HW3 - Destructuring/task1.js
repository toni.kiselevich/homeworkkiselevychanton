const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];


function merge(array1, array2){
    return [...new Set([...array1, ...array2])];
}

console.log(merge(clients1, clients2));
