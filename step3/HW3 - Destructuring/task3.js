const user1 = {
    name: "John",
    years: 30
};

function destructuring(object){
    const {name, years, isAdmin = false} = object;
    console.log(name, years, isAdmin);
}

destructuring(user1)
