class Data {
    render(city, country, regionName, ip) {
        const info = document.createElement('div');
        info.innerHTML = `
        <p>IP: ${ip}</p>
        <p>Country: ${country}</p>
        <p>City: ${city}</p>
        <p>Region: ${regionName}</p>
        `;
        root.append(info)
    }

    async findIP() {
        const request = await axios.get('https://api.ipify.org/?format=json');
        const data = await request.data;
        const {ip} = data;


        this.findData(ip);
    }
    async findData(ip){
        const request = await axios.get(`http://ip-api.com/json/${ip}`);
        const data = await request.data;

        const {city, country, regionName} = data;

        this.render(city, country, regionName, ip);
    }
}

const root = document.querySelector('#root');
const data = new Data();
const btn = document.querySelector('.find-btn');

btn.addEventListener('click', () => {
    data.findIP();
})
