const URL = 'https://ajax.test-danit.com/api/swapi/films';
const root = document.querySelector('#root');

class StarWarsInfo{
    constructor(URL) {
        this.FILM_URL = URL;
    }
    render(root){
        const list = document.createElement('ul');

        this.filmRequest()
            .then(({data}) => {
                data.forEach(({episodeId, name: nameOfEpisode, openingCrawl, characters}) => {
                    const film = document.createElement('li');
                    const nameOfCharacters = []
                    characters.forEach(character => {

                        this.characterRequest(character)
                            .then(({data}) => {
                                const {name: nameOfCharacter} = data;
                                nameOfCharacters.push(nameOfCharacter)
                                film.innerHTML =
                                    `
                                    <p>Episode : ${episodeId}</p>
                                    <p>Name : ${nameOfEpisode}</p>
                                    <p>Characters : ${nameOfCharacters}</p>
                                    <p>Plot : ${openingCrawl}</p>
                                    `;
                            })
                    })

                    list.append(film);
                })
            })
        root.append(list)
    }
    filmRequest(){
        return axios.get(this.FILM_URL);
    }
    characterRequest(CHARACTERS_URL){
            return axios.get(CHARACTERS_URL);
    }
}

const test1 = new StarWarsInfo(URL);

test1.render(root);

