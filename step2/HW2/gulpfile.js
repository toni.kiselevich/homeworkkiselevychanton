const {parallel, series, watch, src, dest} = require("gulp");
const browserSync = require("browser-sync").create();
const sass = require("gulp-sass")(require("sass"));
const minify = require('gulp-minify');
const cleanCSS = require('gulp-clean-css');
const autoprefixer = require('gulp-autoprefixer');
const imagemin = require('gulp-imagemin');
const del = require('del');
const {clean} = require("./gulpfile");



const serv = (cb) => {
    browserSync.init({
        server: {
            baseDir: "./",
        },
        browser: "Chrome",
        open: true,
    });
    cb()
};




const styles = (cb) => {
    src("./src/styles/main.scss")
        .pipe(sass().on("error", sass.logError))
        .pipe(dest("./dist/css"))
        .pipe(browserSync.stream());
    cb();
};

const stylesB = (cb) => {
    src("./src/styles/main.scss")
        .pipe(sass().on("error", sass.logError))
        .pipe(cleanCSS())
        .pipe(
            autoprefixer({
                cascade: true,
                overrideBrowserslist: ['last 5 versions'],
            }),
        )
        .pipe(dest("./dist/css"))
    cb();
};


const scripts = (cb) => {
    src("./src/js/index.js")
        .pipe(dest("./dist/js"))
        .pipe(browserSync.stream());
    cb();
};

const scriptsB = (cb) => {
    src("./src/js/index.js")
        .pipe(minify())
        .pipe(dest("./dist/js"))
    cb();
};

const cleanDist = (cb) => {
    del.sync(['./dist']);
    cb();
}


const cleanJs = (cb) => {
    del.sync(['./dist/js/index.js']);
    cb();
}

const img = (cb) => {
    src('./src/images/**/*.{jpg,jpeg,png,svg}')
        .pipe(dest('./dist/images'));
    cb();
}

const imageMin = (cb) => {
    src('./src/images/**/*.{jpg,jpeg,png,svg}')
        .pipe(
        imagemin([
            imagemin.gifsicle({ interlaced: true }),
            imagemin.mozjpeg({ quality: 75, progressive: true }),
            imagemin.optipng({ optimizationLevel: 5 }),
            imagemin.svgo({
                plugins: [{ removeViewBox: true }, { cleanupIDs: false }],
            }),
        ]),
    )
        .pipe(dest('dist/images'));
    cb();
}


const watcher = (cb) => {
    watch("*.html").on("change", browserSync.reload);
    watch("./src/js/*.js").on("change", series(scripts, browserSync.reload));
    watch("./src/styles/**/*.scss", styles);
    watch("./src/img/**/*.{jpg,jpeg,png,svg}").on("change", series(img, browserSync.reload));
    cb();
};




exports.dev = parallel(serv, watcher, series(styles, scripts, img));
exports.build = parallel(cleanDist,stylesB,scriptsB,imageMin, cleanJs);
exports.clean = cleanJs;
