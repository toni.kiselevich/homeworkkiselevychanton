import React, {Component} from "react";
import Button from "./components/Button";
import Modal from "./components/Modal";
import './App.css'

class App extends Component {
    state = {

        firstOpenButtonData: {
            backgroundColor: "#9932CC",
            text: "Open first modal",
            className: "open-button"
        },
        secondOpenButtonData: {
            backgroundColor: "#DB7093",
            text: "Open second modal",
            className: "open-button"
        },

        isActive: false,

        isFirstModal: false,
        firstModalData: {
            header: "Do you want to delete this file ?",
            closeButton: true,
            text: "Once you delete this file it won't be possible to undo this action. Are you sure you want to delete it?",
            className: "first-modal",
        },

        isSecondModal: false,
        secondModalData: {
            header: "Do you want to save this file ?",
            closeButton: true,
            text: "If you want to save the file, press OK, otherwise the file will not be saved!",
            className: "second-modal",
        }
    }

    openFirstModal = (e) => {
        this.setState((prevState) => {
            return {
                ...prevState,
                isFirstModal: !prevState.isFirstModal,
                isActive: !prevState.isActive
            }
        })
        e.stopPropagation();
        document.addEventListener("click", this.closeModal);
    }


    openSecondModal = (e) => {
        this.setState((prevState) => {
            return {
                ...prevState,
                isSecondModal: !prevState.isSecondModal,
                isActive: !prevState.isActive
            }
        })
        e.stopPropagation();
        document.addEventListener("click", this.closeModal);
    }

    closeModal = (e) => {
        if (e.target.className === "modal-wrapper" || e.target.className === "app-wrapper active") {
            this.setState((prevState) => {
                return {
                    ...prevState,
                    isFirstModal: false,
                    isSecondModal: false,
                    isActive: !prevState.isActive
                }
            })
            document.removeEventListener("click", this.closeModal);
        }
    };

    render() {
        const {
            isFirstModal,
            isSecondModal,
            isActive,
            firstOpenButtonData: firstBtn,
            secondOpenButtonData: secondBtn,
            firstModalData: firstModal,
            secondModalData: secondModal
        } = this.state;

        return (
            <div className={isActive ? "app-wrapper active" : "app-wrapper"}>
                {!isActive && <div className="button-wrapper">
                    <Button backgroundColor={firstBtn.backgroundColor} text={firstBtn.text} className={firstBtn.className} onClickHandler={this.openFirstModal}/>
                    <Button backgroundColor={secondBtn.backgroundColor} text={secondBtn.text} className={secondBtn.className} onClickHandler={this.openSecondModal}/>
                </div>}

                <div className="modal-wrapper">
                    {isFirstModal &&
                        <Modal header={firstModal.header} closeButton={firstModal.closeButton} text={firstModal.text}
                               className={firstModal.className} onClickHandler={this.openFirstModal} actions={
                            <div className="modal-buttons-wrapper">
                                <Button backgroundColor="#b2392a" text="OK" className="modal-button" onClickHandler={this.openFirstModal}/>
                                <Button backgroundColor="#b2392a" text="CANCEL" className="modal-button" onClickHandler={this.openFirstModal}/>
                            </div>
                        }/>}
                    {isSecondModal && <Modal header={secondModal.header} closeButton={secondModal.closeButton} text={secondModal.text}
                                             className={secondModal.className}  onClickHandler={this.openSecondModal} actions={
                        <div className="modal-buttons-wrapper">
                            <Button backgroundColor="#3b923d" text="OK" className="modal-button" onClickHandler={this.openSecondModal}/>
                        </div>
                    }/>}
                </div>
            </div>
        )
    }
}

export default App;
