import React, {Component} from "react";
import './Button.scss';


class Button extends Component{
    render() {
        const {backgroundColor, text, className, onClickHandler} = this.props;

        return(
            <button className={className} type="button" style={{backgroundColor: backgroundColor}} onClick={onClickHandler}>{text}</button>
        )
    }
}

export default Button;
