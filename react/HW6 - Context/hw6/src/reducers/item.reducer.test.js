import {actionSetItems ,actionHandleModal, actionClearCart} from './items.reducer'
import {itemsReducer} from "./index";
import {useDispatch, useSelector} from "react-redux";
import {selectorState} from "../selectors";

describe("testing items reducer", () => {
    test("testing action set item", () => {
        const mockData = [{
            name: "RODE NT1-A",
            price: 11000,
            img: "https://content.rozetka.com.ua/goods/images/big/188330760.jpg",
            article: 23410,
            color: "Gold"
        }];

        const mockDataExpected = {
            items:[{
                name: "RODE NT1-A",
                price: 11000,
                img: "https://content.rozetka.com.ua/goods/images/big/188330760.jpg",
                article: 23410,
                color: "Gold"
            }],
        }

        expect(itemsReducer({}, actionSetItems(mockData))).toStrictEqual(mockDataExpected);
    });

    test("testing action handle modal ", () => {
        const mockData = true;

        const mockDataExpected = {
            isModal: true,
        }

        expect(itemsReducer({}, actionHandleModal(mockData))).toStrictEqual(mockDataExpected);
    })
})
